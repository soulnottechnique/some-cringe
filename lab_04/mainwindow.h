#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "elevator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_floor1Button_clicked();

    void on_floor2Button_clicked();

    void on_floor3Button_clicked();

    void on_floor4Button_clicked();

    void on_floor5Button_clicked();

    void on_floor6Button_clicked();

    void on_floor7Button_clicked();

private:
    Ui::MainWindow *ui;
    Elevator elevator;
};
#endif // MAINWINDOW_H
