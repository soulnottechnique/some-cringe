#ifndef DOORS_H
#define DOORS_H

#include <QObject>
#include <QTimer>
#include <iostream>

#define DOORS_TIMEOUT 5000 // ms
#define DOORS_OPENTIME 3000

enum doorsState {OPENED, CLOSED, OPENING, CLOSING};

class Doors : public QObject
{
    Q_OBJECT
public:
    Doors();

public slots:
    void open();
    void close();

signals:
    void opened();
    void closed();

private slots:
    void openHandler();
    void closeHandler();

private:
    doorsState state;
    QTimer open_timer, close_timer, wait_timer;
};

#endif // DOORS_H
