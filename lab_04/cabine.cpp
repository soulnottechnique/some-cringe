#include "cabine.h"

Cabine::Cabine()
{
    state = WAIT;
    floor = 1;
    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(timeoutHandler()));
    QObject::connect(this, SIGNAL(floorReached()), this, SLOT(targetHandler()));

    std::cout << "cabine created at floor 1 in state: wait" << std::endl;
}

void Cabine::timeoutHandler()
{
    if (state == UP)
        floor += 1;
    else if (state == DOWN)
        floor -= 1;

    std::cout << "cabine reached floor " << floor << std::endl;
    emit floorChanged();

    if (floor == target)
        emit floorReached();
    else if (state == UP)
        moveUp();
    else
        moveDown();
}

void Cabine::targetHandler()
{
    timer.stop();
    state = WAIT;
}

void Cabine::moveUp()
{
    state = UP;
    timer.start(CABINE_TIME);
}

void Cabine::moveDown()
{
    state = DOWN;
    timer.start(CABINE_TIME);
}

void Cabine::moveTo(int floor)
{
    this->target = floor;

    if (this->floor < target)
        moveUp();
    else if (this->floor > target)
        moveDown();
    else
        emit floorReached();
}
