#include "doors.h"

Doors::Doors()
{
    state = CLOSED;
    QObject::connect(&open_timer, SIGNAL(timeout()), this, SLOT(openHandler()));
    QObject::connect(&close_timer, SIGNAL(timeout()), this, SLOT(closeHandler()));
    QObject::connect(&wait_timer, SIGNAL(timeout()), this, SLOT(close()));

    std::cout << "doors created in state: closed" << std::endl;
}

void Doors::open()
{
    if (state == CLOSED)
    {
        state = OPENING;
        std::cout << "doors opening..." << std::endl;
        open_timer.start(DOORS_OPENTIME);
    }
    else
        std::cout << "ignored: open doors" << std::endl;
}

void Doors::close()
{
    if (state == OPENED)
    {
        wait_timer.stop();
        state = CLOSING;
        std::cout << "doors closing..." << std::endl;
        close_timer.start(DOORS_OPENTIME);
    }
    else
        std::cout << "ignored: close doors" << std::endl;
}

void Doors::openHandler()
{
    open_timer.stop();
    state = OPENED;
    std::cout << "doors opened" << std::endl;
    wait_timer.start(DOORS_TIMEOUT);
    emit opened();
}

void Doors::closeHandler()
{
    close_timer.stop();
    state = CLOSED;
    std::cout << "doors closed" << std::endl;
    emit closed();
}

