#ifndef CABINE_H
#define CABINE_H

#include <iostream>
#include <QObject>
#include <QTimer>

#define CABINE_TIME 2000 // ms

enum cabineState {UP, DOWN, WAIT};

class Cabine : public QObject
{
    Q_OBJECT
public:
    Cabine();

public slots:
    void moveTo(int floor);

signals:
    void floorReached();
    void floorChanged();

private slots:
    void timeoutHandler();
    void targetHandler();

private:
    void moveUp();
    void moveDown();

    QTimer timer;
    cabineState state;
    int target;
    int floor;
};

#endif // CABINE_H
