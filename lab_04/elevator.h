#ifndef ELEVATOR_H
#define ELEVATOR_H

#include <QObject>
#include <QTimer>
#include <QQueue>

#include "doors.h"
#include "cabine.h"

enum elevatorState {FREE, BUSY};

class Elevator : public QObject
{
    Q_OBJECT
public:
    Elevator();

    void addTarget(int floor);

signals:
    void targetAppear();
    void allComplete();

private slots:
    void targetComplete();
    void execute();

private:
    Doors doors;
    Cabine cabine;

    QQueue<int> queue;
    elevatorState state;
};

#endif // ELEVATOR_H
