#include "elevator.h"

Elevator::Elevator()
{
    state = FREE;
    connect(&cabine, SIGNAL(floorReached()), &doors, SLOT(open()));
    connect(&doors, SIGNAL(closed()), this, SLOT(targetComplete()));
    connect(this, SIGNAL(targetAppear()), this, SLOT(execute()));

    std::cout << "elevator created in state: free\n" << std::endl;
}

void Elevator::addTarget(int floor)
{
    queue.push_back(floor);

    if (state == FREE)
        emit targetAppear();
}

void Elevator::targetComplete()
{
    if (queue.isEmpty())
        state = FREE;
    else
        emit targetAppear();
}

void Elevator::execute()
{
    state = BUSY;
    cabine.moveTo(queue.dequeue());
}
