#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_floor1Button_clicked()
{
    elevator.addTarget(1);
}

void MainWindow::on_floor2Button_clicked()
{
    elevator.addTarget(2);
}

void MainWindow::on_floor3Button_clicked()
{
    elevator.addTarget(3);
}

void MainWindow::on_floor4Button_clicked()
{
    elevator.addTarget(4);
}

void MainWindow::on_floor5Button_clicked()
{
    elevator.addTarget(5);
}

void MainWindow::on_floor6Button_clicked()
{
    elevator.addTarget(6);
}

void MainWindow::on_floor7Button_clicked()
{
    elevator.addTarget(7);
}
