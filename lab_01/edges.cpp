#include "edges.h"

ret_code_t read_edges(edges_t &edg, FILE *file)
{
    ret_code_t rc = SUCCESS;

    for (size_t i = 0; i < edg.count && rc == SUCCESS; i++)
        rc = read_edge(edg.arr[i], file);

    return rc;
}

ret_code_t realloc_edges(edges_t &edges, size_t count)
{
    ret_code_t rc = SUCCESS;

    edge_t *edg_tmp = (edge_t *)realloc(edges.arr, count * sizeof (edge_t));
    if (edg_tmp)
    {
        edges.arr = edg_tmp;
        edges.count = count;
    }
    else
        rc = ALLOC_ERR;

    return rc;
}

void copy_edges(edges_t &dst, edges_t src)
{
    for (size_t i = 0; i < src.count; i++)
        dst.arr[i] = src.arr[i];
}

void init_edges(edges_t &edges)
{
    edges.count = 0;
    edges.arr = NULL;
}
