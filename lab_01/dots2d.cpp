#include "dots2d.h"

ret_code_t realloc_dots2d(dots2d_t &dots, size_t count)
{
    ret_code_t rc = SUCCESS;

    dot2d_t *vert_tmp = (dot2d_t *)realloc(dots.arr, count * sizeof (dot2d_t));
    if (vert_tmp)
    {
        dots.arr = vert_tmp;
        dots.count = count;
    }
    else
        rc = ALLOC_ERR;

    return rc;
}
