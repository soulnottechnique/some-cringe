#ifndef EDGES_H
#define EDGES_H

#include <stdlib.h>
#include <stdio.h>
#include "ret_codes.h"
#include "edge.h"

typedef struct
{
    edge_t *arr;
    size_t count;
} edges_t;

ret_code_t read_edges(edges_t &edg, FILE *file);

ret_code_t realloc_edges(edges_t &edges, size_t count);

void copy_edges(edges_t &dst, edges_t src);

void init_edges(edges_t &edges);

#endif // EDGES_H
