#ifndef DOTS3D_H
#define DOTS3D_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "ret_codes.h"
#include "dots2d.h"
#include "dot3d.h"

typedef struct
{
    dot3d_t *arr;
    size_t count;
} dots3d_t;

ret_code_t read_dots(dots3d_t &dots, FILE *file);

ret_code_t realloc_dots3d(dots3d_t &dots, size_t count);

void rotate_dots_x(dots3d_t &dots, double angle);

void rotate_dots_y(dots3d_t &dots, double angle);

void rotate_dots_z(dots3d_t &dots, double angle);

void shift_dots(dots3d_t &dots, dot3d_t delta);

void scale_dots(dots3d_t &dots, dot3d_t center, double k);

void project_dots(dots2d_t &proj, dots3d_t src);

void init_dots(dots3d_t &dots);

dot3d_t get_dots_center(dots3d_t dots);

#endif // DOTS3D_H
