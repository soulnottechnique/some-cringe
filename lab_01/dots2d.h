#ifndef DOTS2D_H
#define DOTS2D_H

#include <stdlib.h>
#include "ret_codes.h"
#include "dot2d.h"

typedef struct
{
    dot2d_t *arr;
    size_t count;
} dots2d_t;

ret_code_t realloc_dots2d(dots2d_t &dots, size_t count);

#endif // DOTS2D_H
