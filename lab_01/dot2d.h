#ifndef DOT2D_H
#define DOT2D_H

typedef struct
{
    double x, y;
} dot2d_t;

#endif // DOT2D_H
