#include "dot3d.h"

ret_code_t read_dot(dot3d_t &dot, FILE *file)
{
    if (fscanf(file, "%lf%lf%lf", &dot.x, &dot.y, &dot.z) != 3)
        return BAD_FILE;

    return SUCCESS;
}

void rotate_dot_x(dot3d_t &dot, double cos_a, double sin_a)
{
    double old_y = dot.y;
    dot.y = dot.y * cos_a + dot.z * sin_a;
    dot.z = -old_y * sin_a + dot.z * cos_a;
}

void rotate_dot_y(dot3d_t &dot, double cos_a, double sin_a)
{
    double old_x = dot.x;
    dot.x = dot.x * cos_a + dot.z * sin_a;
    dot.z = -old_x * sin_a + dot.z * cos_a;
}

void rotate_dot_z(dot3d_t &dot, double cos_a, double sin_a)
{
    double old_x = dot.x;
    dot.x = dot.x * cos_a + dot.y * sin_a;
    dot.y = -old_x * sin_a + dot.y * cos_a;
}

void shift_dot(dot3d_t &dot, dot3d_t delta)
{
    dot.x += delta.x;
    dot.y += delta.y;
    dot.z += delta.z;
}

void scale_dot(dot3d_t &dot, dot3d_t center, double k)
{
    dot.x = k * dot.x + center.x * (1.0 - k);
    dot.y = k * dot.y + center.y * (1.0 - k);
    dot.z = k * dot.z + center.z * (1.0 - k);
}

void project_dot(dot2d_t &proj, dot3d_t src)
{
    const double k = sqrt(2.0) / 4.0;

    double delta = k * src.y;
    proj.x = src.x - delta;
    proj.y = src.z - delta;
}

void neg_dot(dot3d_t &dot)
{
    dot.x = -dot.x;
    dot.y = -dot.y;
    dot.z = -dot.z;
}
