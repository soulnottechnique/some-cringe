#ifndef DOT3D_H
#define DOT3D_H

#include "ret_codes.h"
#include "stdio.h"
#include "dot2d.h"
#include "math.h"

typedef struct
{
    double x, y, z;
} dot3d_t;

ret_code_t read_dot(dot3d_t &dot, FILE *file);

void rotate_dot_y(dot3d_t &dot, double cos_a, double sin_a);

void rotate_dot_x(dot3d_t &dot, double cos_a, double sin_a);

void rotate_dot_z(dot3d_t &dot, double cos_a, double sin_a);

void shift_dot(dot3d_t &dot, dot3d_t delta);

void scale_dot(dot3d_t &dot, dot3d_t center, double k);

void project_dot(dot2d_t &proj, dot3d_t src);

void neg_dot(dot3d_t &dot);

#endif // DOT3D_H
