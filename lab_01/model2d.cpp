#include "model2d.h"

ret_code_t create_model2d(model2d_t &model, size_t vert, size_t edg)
{
    if (realloc_dots2d(model.dots, vert) == SUCCESS)
    {
        if (realloc_edges(model.edges, edg) == SUCCESS)
            return SUCCESS;
        else
        {
            realloc_dots2d(model.dots, 0);
            return ALLOC_ERR;
        }
    }
    else
        return ALLOC_ERR;
}

ret_code_t destroy_model2d(model2d_t &model)
{    
    realloc_dots2d(model.dots, 0);
    realloc_edges(model.edges, 0);

    return SUCCESS;
}
