#ifndef PARAMS_H
#define PARAMS_H

#include "model2d.h"
#include "dot3d.h"

typedef struct
{
    char *filename;
} load_param_t;

typedef struct
{
    dot3d_t delta;
} shift_param_t;

typedef struct
{
    double alpha, beta, gamma;
} rotate_param_t;

typedef struct
{
    double k;
} scale_param_t;

typedef struct
{

} destroy_param_t;

typedef struct
{

} projection_param_t;

#endif // PARAMS_H
