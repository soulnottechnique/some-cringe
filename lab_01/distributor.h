#ifndef DISTRIBUTOR_H
#define DISTRIBUTOR_H

#include "ret_codes.h"
#include "params.h"
#include "model3d.h"

typedef enum
{
    LOAD,
    SHIFT,
    ROTATE,
    SCALE,
    DESTROY,
    PROJECT
} task_t;

typedef union
{
    load_param_t load_params;
    shift_param_t shift_params;
    rotate_param_t rotate_params;
    scale_param_t scale_params;
    destroy_param_t destroy_params;
    projection_param_t projection_params;
} params_t;

ret_code_t distributor(model2d_t &model2d, const task_t task, params_t &params);

#endif // DISTRIBUTOR_H
