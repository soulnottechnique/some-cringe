#include "model3d.h"

static double degr_to_rad(double angle)
{
    return angle / 180.0 * 3.1415926;
}

static ret_code_t read_count(FILE *file, size_t &vert, size_t &edg)
{
    ret_code_t rc = SUCCESS;

    if (fscanf(file, "%zu%zu", &vert, &edg) != 2)
        rc = BAD_FILE;
    return rc;
}

static ret_code_t alloc_model(model3d_t &model, size_t vert, size_t edg)
{
    ret_code_t rc = SUCCESS;

    if (realloc_dots3d(model.dots, vert) == SUCCESS)
    {
        if (realloc_edges(model.edges, edg) != SUCCESS)
        {
            realloc_dots3d(model.dots, 0);
            rc = ALLOC_ERR;
        }
    }
    else
        rc = ALLOC_ERR;

    return rc;
}

static ret_code_t read_data(model3d_t &model, FILE *file)
{
    ret_code_t rc = read_dots(model.dots, file);

    if (rc == SUCCESS)
        rc = read_edges(model.edges, file);

    return rc;
}

ret_code_t load_model(model3d_t &model, load_param_t &params)
{
    FILE *file = fopen(params.filename, "r");
    if (!file) return CANT_OPEN_FILE;

    ret_code_t rc = SUCCESS;
    size_t vert, edg;

    model3d_t tmp_model = init_model();

    rc = read_count(file, vert, edg);

    if (rc == SUCCESS)
    {
        rc = alloc_model(tmp_model, vert, edg);
        if (rc == SUCCESS)
        {
            rc = read_data(tmp_model, file);

            if (rc != SUCCESS)
                destroy_model(tmp_model);
        }
    }

    fclose(file);

    if (rc == SUCCESS)
    {
        destroy_model(model);
        model = tmp_model;
    }

    return rc;
}

ret_code_t shift_model(model3d_t &model, shift_param_t params)
{
    shift_dots(model.dots, params.delta);
    return SUCCESS;
}

static dot3d_t get_model_center(model3d_t &model)
{
    return get_dots_center(model.dots);
}

ret_code_t rotate_model(model3d_t &model, rotate_param_t params)
{
    dot3d_t center = get_model_center(model);

    neg_dot(center);
    shift_dots(model.dots, center);

    rotate_dots_x(model.dots, degr_to_rad(params.alpha));
    rotate_dots_y(model.dots, degr_to_rad(params.beta));
    rotate_dots_z(model.dots, degr_to_rad(params.gamma));

    neg_dot(center);
    shift_dots(model.dots, center);

    return SUCCESS;
}

ret_code_t scale_model(model3d_t &model, scale_param_t params)
{
    dot3d_t center = get_model_center(model);

    scale_dots(model.dots, center, params.k);

    return SUCCESS;
}

ret_code_t get_projection(model2d_t &model2d, model3d_t &model, projection_param_t &params)
{
    ret_code_t rc = SUCCESS;

    rc = create_model2d(model2d, model.dots.count, model.edges.count);

    if (rc == SUCCESS)
    {
        project_dots(model2d.dots, model.dots);
        copy_edges(model.edges, model2d.edges);
    }
    return rc;
}

ret_code_t destroy_model(model3d_t &model)
{
    realloc_dots3d(model.dots, 0);
    realloc_edges(model.edges, 0);

    return SUCCESS;
}

model3d_t init_model(void)
{
    model3d_t model;

    init_dots(model.dots);
    init_edges(model.edges);

    return model;
}
