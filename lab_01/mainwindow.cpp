#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "distributor.h"

#define IMG_SIZE_X 400
#define IMG_SIZE_Y 400

model2d_t model2d;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    params_t params;

    params.destroy_params = {};
    distributor(model2d, DESTROY, params);

    delete ui;
}

void update_screen(Ui::MainWindow *ui)
{
    static QGraphicsScene scene;

    QImage image = QImage(IMG_SIZE_X, IMG_SIZE_Y, QImage::Format_RGB32);
    QPainter painter(&image);
    painter.setPen(QColor(255, 0, 0));
    image.fill(0);

    size_t from, to;
    for (size_t i = 0; i < model2d.edges.count; i++)
    {
        from = model2d.edges.arr[i].from;
        to = model2d.edges.arr[i].to;

        QPoint a(model2d.dots.arr[from].x, IMG_SIZE_Y - model2d.dots.arr[from].y);
        QPoint b(model2d.dots.arr[to].x, IMG_SIZE_Y - model2d.dots.arr[to].y);

        painter.drawLine(a, b);
    }

    QPixmap pixmap = QPixmap::fromImage(image);
    scene.addPixmap(pixmap);

    ui->graphicsView->setScene(&scene);
    ui->graphicsView->show();
}

void MainWindow::on_loadButton_clicked()
{
    QString filename = ui->filename_line->text();
    params_t params = {};
    params.load_params.filename = filename.toLocal8Bit().data();

    ret_code_t rc = distributor(model2d, LOAD, params);

    if (rc == SUCCESS)
    {
        model2d = {{NULL, 0}, {NULL, 0}};
        params.projection_params = {};
        rc = distributor(model2d, PROJECT, params);

        if (rc == SUCCESS)
        {
            update_screen(ui);
            destroy_model2d(model2d);
        }
        else
            ui->statusbar->showMessage(QString("proj error"));
    }
    else
        ui->statusbar->showMessage(QString("read error"));
}

void MainWindow::on_shiftButton_clicked()
{
    bool ok = true;

    double dx = ui->shiftline_dx->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error dx");
        return ;
    }
    double dy = ui->shiftline_dy->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error dy");
        return ;
    }
    double dz = ui->shiftline_dz->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error dz");
        return ;
    }

    params_t params = {};
    params.shift_params = {dx, dy, dz};

    ret_code_t rc = distributor(model2d, SHIFT, params);

    model2d = {{NULL, 0}, {NULL, 0}};
    params.projection_params = {};
    if (rc == SUCCESS)
            rc = distributor(model2d, PROJECT, params);

    if (rc == SUCCESS)
    {
        update_screen(ui);
        destroy_model2d(model2d);
    }
    else
        ui->statusbar->showMessage("shift error");
}

void MainWindow::on_rotateButton_clicked()
{
    bool ok = true;

    double alpha = ui->rotatealpha_line->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error alpha");
        return ;
    }
    double beta = ui->rotatebeta_line->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error beta");
        return ;
    }
    double gamma = ui->rotategamma_line->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error gamma");
        return ;
    }

    params_t params = {};
    params.rotate_params = {alpha, beta, gamma};

    ret_code_t rc = distributor(model2d, ROTATE, params);

    model2d = {{NULL, 0}, {NULL, 0}};
    params.projection_params = {};
    if (rc == SUCCESS)
            rc = distributor(model2d, PROJECT, params);

    if (rc == SUCCESS)
    {
        update_screen(ui);
        destroy_model2d(model2d);
    }
    else
        ui->statusbar->showMessage("rotate error");
}

void MainWindow::on_scaleButton_clicked()
{
    bool ok = true;

    double k = ui->scale_line->text().toDouble(&ok);
    if (!ok)
    {
        ui->statusbar->showMessage("error k");
        return ;
    }

    params_t params = {};
    params.scale_params = {k};

    ret_code_t rc = distributor(model2d, SCALE, params);

    model2d = {{NULL, 0}, {NULL, 0}};
    params.projection_params = {};
    if (rc == SUCCESS)
            rc = distributor(model2d, PROJECT, params);

    if (rc == SUCCESS)
    {
        update_screen(ui);
        destroy_model2d(model2d);
    }
    else
        ui->statusbar->showMessage("scale error");
}
