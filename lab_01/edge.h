#ifndef EDGE_H
#define EDGE_H

#include "stdlib.h"
#include "stdio.h"
#include "ret_codes.h"

typedef struct
{
    size_t from, to;
} edge_t;

ret_code_t read_edge(edge_t &edg, FILE *file);


#endif // EDGE_H
