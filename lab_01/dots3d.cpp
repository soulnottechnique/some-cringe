#include "dots3d.h"

ret_code_t read_dots(dots3d_t &dots, FILE *file)
{
    ret_code_t rc = SUCCESS;

    for (size_t i = 0; (i < dots.count) && (rc == SUCCESS); i++)
        rc = read_dot(dots.arr[i], file);

    return rc;
}

ret_code_t realloc_dots3d(dots3d_t &dots, size_t count)
{
    ret_code_t rc = SUCCESS;

    dot3d_t *vert_tmp = (dot3d_t *)realloc(dots.arr, count * sizeof (dot3d_t));
    if (vert_tmp)
    {
        dots.arr = vert_tmp;
        dots.count = count;
    }
    else
        rc = ALLOC_ERR;

    return rc;
}

void rotate_dots_y(dots3d_t &dots, double angle)
{
    double cos_a = cos(angle);
    double sin_a = sin(angle);

    for (size_t i = 0; i < dots.count; i++)
        rotate_dot_y(dots.arr[i], cos_a, sin_a);
}

void rotate_dots_x(dots3d_t &dots, double angle)
{
    double cos_a = cos(angle);
    double sin_a = sin(angle);

    for (size_t i = 0; i < dots.count; i++)
        rotate_dot_x(dots.arr[i], cos_a, sin_a);
}

void rotate_dots_z(dots3d_t &dots, double angle)
{
    double cos_a = cos(angle);
    double sin_a = sin(angle);

    for (size_t i = 0; i < dots.count; i++)
        rotate_dot_z(dots.arr[i], cos_a, sin_a);
}

void shift_dots(dots3d_t &dots, dot3d_t delta)
{
    for (size_t i = 0; i < dots.count; i++)
        shift_dot(dots.arr[i], delta);
}

void scale_dots(dots3d_t &dots, dot3d_t center, double k)
{
    for (size_t i = 0; i < dots.count; i++)
        scale_dot(dots.arr[i], center, k);
}

void project_dots(dots2d_t &proj, dots3d_t src)
{
    for (size_t i = 0; i < src.count; i++)
        project_dot(proj.arr[i], src.arr[i]);
}

dot3d_t get_dots_center(dots3d_t dots)
{
    double sx = 0.0, sy = 0.0, sz = 0.0;
    for (size_t i = 0; i < dots.count; i++)
    {
        sx += dots.arr[i].x;
        sy += dots.arr[i].y;
        sz += dots.arr[i].z;
    }

    return {sx / dots.count, sy / dots.count, sz / dots.count};
}

void init_dots(dots3d_t &dots)
{
    dots.count = 0;
    dots.arr = NULL;
}



