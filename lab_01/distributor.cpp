#include "distributor.h"

ret_code_t distributor(model2d_t &model2d, const task_t task, params_t &params)
{
    ret_code_t rc;

    static model3d_t model = init_model();

    switch (task)
    {
    case LOAD:
        rc = load_model(model, params.load_params);
        break;

    case SHIFT:
        rc = shift_model(model, params.shift_params);
        break;

    case ROTATE:
        rc = rotate_model(model, params.rotate_params);
        break;

    case SCALE:
        rc = scale_model(model, params.scale_params);
        break;

    case DESTROY:
        rc = destroy_model(model);
        break;

    case PROJECT:
        rc = get_projection(model2d, model, params.projection_params);
        break;

    default:
        rc = UNKNOWN_TASK;
        break;
    }

    return rc;
}
