#ifndef MODEL3D_H
#define MODEL3D_H

#include <stdio.h>
#include <math.h>
#include "edges.h"
#include "dots3d.h"
#include "ret_codes.h"
#include "params.h"
#include "model2d.h"

typedef struct
{
    dots3d_t dots;
    edges_t edges;
} model3d_t;

model3d_t init_model(void);

ret_code_t load_model(model3d_t &model, load_param_t &params);

ret_code_t shift_model(model3d_t &model, shift_param_t params);

ret_code_t rotate_model(model3d_t &model, rotate_param_t params);

ret_code_t scale_model(model3d_t &model, scale_param_t params);

ret_code_t get_projection(model2d_t &model2d, model3d_t &model, projection_param_t &params);

ret_code_t destroy_model(model3d_t &model);

#endif // MODEL3D_H
