#ifndef RET_CODES_H
#define RET_CODES_H

typedef enum
{
    SUCCESS,
    ALLOC_ERR,
    UNKNOWN_TASK,
    CANT_OPEN_FILE,
    BAD_FILE
} ret_code_t;

#endif // RET_CODES_H
