#include "edge.h"

ret_code_t read_edge(edge_t &edg, FILE *file)
{
    if (fscanf(file, "%zu%zu", &edg.from, &edg.to) != 2)
        return BAD_FILE;

    return SUCCESS;
}
