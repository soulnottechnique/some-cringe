#ifndef MODEL2D_H
#define MODEL2D_H

#include "dots2d.h"
#include "edges.h"

typedef struct
{
    dots2d_t dots;
    edges_t edges;
} model2d_t;

ret_code_t create_model2d(model2d_t &model, size_t vert, size_t edg);

ret_code_t destroy_model2d(model2d_t &model);

#endif // MODEL2D_H
