#ifndef SCENE_SCENE_BUILDER_H
#define SCENE_SCENE_BUILDER_H

#include "scene/scene.h"

class SceneBuilder {
public:
    SceneBuilder();

    bool isBuild();

    void buildName(std::string name);
    void buildObject(std::shared_ptr<Object> obj);

    std::shared_ptr<Scene> get();
    void reset();

private:
    std::shared_ptr<Scene> scene;
};

#endif
