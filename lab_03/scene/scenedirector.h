#ifndef SCENE_SCENE_DIRECTOR_H
#define SCENE_SCENE_DIRECTOR_H

#include "scene/basescenedirector.h"
#include "scene/scenebuilder.h"
#include "manager/loadmanagercreator.h"

class SceneDirector : public BaseSceneDirector {
public:
    SceneDirector(std::shared_ptr<SourceLoader> source);

    ~SceneDirector() = default;

    virtual std::shared_ptr<Scene> load() override;

private:
    std::shared_ptr<SceneBuilder> builder;
};

#endif
