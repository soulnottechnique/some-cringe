#ifndef SCENE_SCENE_H
#define SCENE_SCENE_H

#include <string>
#include "object/composite.h"
#include "object/camera.h"

class Scene {
public:
    Scene(std::string name);

    void add(Composite::objPtr &obj);
    void remove(Composite::objIterator &obj);

    void setName(std::string name);

    Composite::objIterator begin();
    Composite::objIterator end();

private:
    std::string name;
    std::shared_ptr<Composite> composite;
    //Composite composite;
    //std::shared_ptr<Camera> watcher;
};

#endif
