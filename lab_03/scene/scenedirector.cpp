#include "scene/scenedirector.h"

SceneDirector::SceneDirector(std::shared_ptr<SourceLoader> source) :
    BaseSceneDirector(source)
{
    builder = std::make_shared<SceneBuilder>();
}

std::shared_ptr<Scene> SceneDirector::load()
{
    source->open();

    if (!source->is_open())
        return nullptr;

    builder->buildName(source->readString());

    std::string modFile = source->readString();
    std::string camFile = source->readString();

    source->close();

    LoadManagerCreator mgr;

    std::shared_ptr<LoadManager> camLoader = mgr.create("camera.txt", camFile);
    std::shared_ptr<LoadManager> modLoader = mgr.create("model.txt", modFile);

    std::shared_ptr<Object> cam = camLoader->load();

    builder->buildObject(cam);
    builder->buildObject(modLoader->load());

    return builder->get();
}
