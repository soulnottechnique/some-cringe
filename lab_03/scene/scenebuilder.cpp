#include "scene/scenebuilder.h"

SceneBuilder::SceneBuilder() :
    scene(std::make_shared<Scene>("default")) {}

bool SceneBuilder::isBuild()
{
    return true;
}

void SceneBuilder::buildName(std::string name)
{
    scene->setName(name);
}

void SceneBuilder::buildObject(std::shared_ptr<Object> obj)
{
    scene->add(obj);
}

std::shared_ptr<Scene> SceneBuilder::get()
{
    return scene;
}

void SceneBuilder::reset()
{
    scene = std::make_shared<Scene>("default");
}
