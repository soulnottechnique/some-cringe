#ifndef SCENE_BASE_SCENE_DIRECTOR_H
#define SCENE_BASE_SCENE_DIRECTOR_H

#include "scene/scenebuilder.h"
#include "load/sourceloader.h"

class BaseSceneDirector {
public:
    BaseSceneDirector(std::shared_ptr<SourceLoader> source) :
        source(source) {}

    virtual ~BaseSceneDirector() = default;

    virtual std::shared_ptr<Scene> load() = 0;

protected:
    std::shared_ptr<SourceLoader> source;
};

#endif
