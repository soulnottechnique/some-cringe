#include "scene/scene.h"

Scene::Scene(std::string name) :
    name(name), composite(std::make_shared<Composite>()) {}

void Scene::setName(std::string name)
{
    this->name = name;
}

void Scene::add(Composite::objPtr &obj)
{
    composite->add(obj);
}

void Scene::remove(Composite::objIterator &obj)
{
    composite->remove(obj);
}


Composite::objIterator Scene::begin()
{
    return composite->begin();
}

Composite::objIterator Scene::end()
{
    return composite->end();
}
