#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "command/facade.h"
#include "draw/drawer.h"
#include "manager/scenemanagercreator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_loadButton_clicked();

    void on_moveButton_clicked();

    void on_scaleButton_clicked();

    void on_rotateButton_clicked();

private:
    void drawThat();

    Ui::MainWindow *ui;
    Facade facade;

    std::shared_ptr<QImage> image;
    std::shared_ptr<Drawer> drawer;

    std::shared_ptr<SceneManager> sceneManager;
};
#endif // MAINWINDOW_H
