#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "draw/qtfactory.h"
#include "command/facade.h"
#include "command/drawcommand.h"
#include "command/loadcommand.h"
#include "command/movecommand.h"
#include "command/rotatecommand.h"
#include "command/scalecommand.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this); 

    image = std::make_shared<QImage>(800, 800, QImage::Format_RGB32);

    QtFactory factory(image, std::make_shared<QPainter>(&*image), Qt::red);
    drawer = factory.create();

    SceneManagerCreator creator;
    sceneManager = creator.create();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawThat()
{
    static QGraphicsScene scene;

    facade.executeCommand(std::make_shared<DrawCommand>(drawer, sceneManager));

    QPixmap pixmap = QPixmap::fromImage(*image);
    scene.addPixmap(pixmap);

    ui->graphicsView->setScene(&scene);
    ui->graphicsView->show();
}

void MainWindow::on_loadButton_clicked()
{
    std::string path = ui->scenePath->text().toStdString();

    facade.executeCommand(std::make_shared<LoadCommand>(path, sceneManager));

    drawThat();
}

void MainWindow::on_moveButton_clicked()
{
    std::string name = ui->objName->text().toStdString();

    float x = ui->lineDX->text().toFloat();
    float y = ui->lineDY->text().toFloat();
    float z = ui->lineDZ->text().toFloat();

    facade.executeCommand(std::make_shared<MoveCommand>(name, Dot(x, y, z), sceneManager));

    drawThat();
}

void MainWindow::on_scaleButton_clicked()
{
    std::string name = ui->objName->text().toStdString();

    float x = ui->lineX->text().toFloat();
    float y = ui->lineY->text().toFloat();
    float z = ui->lineZ->text().toFloat();

    float dx = ui->lineDX->text().toFloat();
    float dy = ui->lineDY->text().toFloat();
    float dz = ui->lineDZ->text().toFloat();

    facade.executeCommand(
           std::make_shared<ScaleCommand>(name, Dot(x, y, z), Dot(dx, dy, dz), sceneManager));

    drawThat();
}

void MainWindow::on_rotateButton_clicked()
{
    std::string name = ui->objName->text().toStdString();

    float x = ui->lineX->text().toFloat();
    float y = ui->lineY->text().toFloat();
    float z = ui->lineZ->text().toFloat();

    float dx = ui->lineDX->text().toFloat();
    float dy = ui->lineDY->text().toFloat();
    float dz = ui->lineDZ->text().toFloat();

    facade.executeCommand(
                std::make_shared<RotateCommand>(name, Dot(x, y, z), Dot(dx, dy, dz), sceneManager));

    drawThat();
}
