#include "solution/modelfileloadercreator.h"

std::shared_ptr<LoadDirector> ModelFileLoaderCreator::createLoader(std::string sourceName)
{
    std::shared_ptr<LoadDirector> director = std::make_shared<ModelDirector>();

    std::shared_ptr<SourceLoader> loader = std::make_shared<FileLoader>(sourceName);

    std::shared_ptr<SourceDecorator> dec = std::make_shared<FileModelLoader>(loader);

    director->setLoader(dec);

    return director;
}
