#include "solution/camerafileloadercreator.h"

std::shared_ptr<LoadDirector> CameraFileLoaderCreator::createLoader(std::string sourceName)
{
    std::shared_ptr<LoadDirector> director = std::make_shared<CameraDirector>();

    std::shared_ptr<SourceLoader> loader = std::make_shared<FileLoader>(sourceName);

    std::shared_ptr<SourceDecorator> dec = std::make_shared<FileCameraLoader>(loader);

    director->setLoader(dec);

    return director;
}
