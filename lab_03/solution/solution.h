#ifndef SOLUTION_SOLUTION_H
#define SOLUTION_SOLUTION_H

#include <memory>
#include <map>
#include "solution/loadercreator.h"

class Solution {
public:
    Solution() = default;

    virtual ~Solution() = default;

    typedef std::unique_ptr<LoaderCreator> (*CreateCreator)();

    bool registration(std::string id, CreateCreator c);
    std::unique_ptr<LoaderCreator> create(std::string id);

protected:
    using CallBackMap = std::map<std::string, CreateCreator>;

    CallBackMap callbacks;
};

#endif
