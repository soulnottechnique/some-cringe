#ifndef SOLUTION_MODEL_FILE_LOADER_CREATOR_H
#define SOLUTION_MODEL_FILE_LOADER_CREATOR_H

#include "solution/loadercreator.h"
#include "load/modeldirector.h"
#include "load/fileloader.h"
#include "load/filemodelloader.h"

class ModelFileLoaderCreator : public LoaderCreator {
public:
    ModelFileLoaderCreator() = default;

    ~ModelFileLoaderCreator() = default;

    virtual std::shared_ptr<LoadDirector> createLoader(std::string sourceName) override;
};

#endif
