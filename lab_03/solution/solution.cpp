#include "solution/solution.h"

bool Solution::registration(std::string id, CreateCreator c)
{
    return callbacks.insert(CallBackMap::value_type(id, c)).second;
}

std::unique_ptr<LoaderCreator> Solution::create(std::string id)
{
    CallBackMap::const_iterator it = callbacks.find(id);

    if (it != callbacks.end())
        return std::unique_ptr<LoaderCreator>((it->second)());

    return nullptr;
}
