#include "solution/registrator.h"

static std::unique_ptr<LoaderCreator> createModelCreator()
{
    return std::unique_ptr<LoaderCreator>(new ModelFileLoaderCreator);
}

static std::unique_ptr<LoaderCreator> createCameraCreator()
{
    return std::unique_ptr<LoaderCreator>(new CameraFileLoaderCreator);
}

void Registrator::makeRegistration(Solution &sol)
{
    bool reg = sol.registration("model:frame:file", createModelCreator);
    //if (!reg) throw

    reg = sol.registration("camera:ortho:file", createCameraCreator);
    //if (!reg) throw
}
