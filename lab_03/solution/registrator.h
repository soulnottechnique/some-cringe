#ifndef SOLUTION_REGISTRATOR_H
#define SOLUTION_REGISTRATOR_H

#include "solution/solution.h"
#include "solution/camerafileloadercreator.h"
#include "solution/modelfileloadercreator.h"

class BaseRegistrator {
public:
    BaseRegistrator() = default;

    virtual ~BaseRegistrator() = default;

    virtual void makeRegistration(Solution &sol) = 0;
};

class Registrator : public BaseRegistrator {
public:
    Registrator() = default;

    virtual ~Registrator() = default;

    virtual void makeRegistration(Solution &sol) override;
};

#endif
