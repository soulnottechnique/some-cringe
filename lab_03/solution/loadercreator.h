#ifndef SOLUTION_LOADER_CREATOR_H
#define SOLUTION_LOADER_CREATOR_H

#include <memory>
#include "load/loaddirector.h"

class LoaderCreator {
public:
    LoaderCreator() = default;

    virtual ~LoaderCreator() = default;

    virtual std::shared_ptr<LoadDirector> createLoader(std::string sourceName) = 0;
};

#endif
