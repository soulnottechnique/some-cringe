#ifndef SOLUTION_CAMERA_FILE_LOADER_CREATOR_H
#define SOLUTION_CAMERA_FILE_LOADER_CREATOR_H

#include "solution/loadercreator.h"
#include "load/cameradirector.h"
#include "load/fileloader.h"
#include "load/filecameraloader.h"

class CameraFileLoaderCreator : public LoaderCreator {
public:
    CameraFileLoaderCreator() = default;

    ~CameraFileLoaderCreator() = default;

    virtual std::shared_ptr<LoadDirector> createLoader(std::string sourceName) override;
};

#endif
