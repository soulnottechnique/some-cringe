#ifndef DRAW_QT_DRAWER_H
#define DRAW_QT_DRAWER_H

#include <QPainter>
#include <memory>
#include "draw/drawer.h"

class QtDrawer : public Drawer{
public:
    QtDrawer(std::shared_ptr<QPainter> painter, std::shared_ptr<QImage> image, QColor pen):
        painter(painter), image(image), pen(pen) {}

    virtual ~QtDrawer() = default;

    virtual void drawLine(int x1, int y1, int x2, int y2) override;
    virtual void clearCanvas() override;

private:
    std::shared_ptr<QPainter> painter;
    std::shared_ptr<QImage> image;
    QColor pen;
};

#endif
