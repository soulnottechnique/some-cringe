#ifndef DRAW_ABSTRACT_FACTORY_H
#define DRAW_ABSTRACT_FACTORY_H

#include <memory>
#include "draw/drawer.h"


class AbstractFactory {
public:
    AbstractFactory() = default;
    virtual ~AbstractFactory() = default;
    virtual std::unique_ptr<Drawer> create() = 0;
};

#endif
