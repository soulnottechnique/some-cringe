#include "draw/qtfactory.h"

std::unique_ptr<Drawer> QtFactory::create()
{
    return std::make_unique<QtDrawer>(painter, img, pen);
}
