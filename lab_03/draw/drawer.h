#ifndef DRAW_DRAWER_H
#define DRAW_DRAWER_H

class Drawer {
public:
    Drawer() = default;
    virtual ~Drawer() = default;

    virtual void drawLine(int x1, int y1, int x2, int y2) = 0;
    virtual void clearCanvas() = 0;
};

#endif
