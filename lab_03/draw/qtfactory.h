#ifndef DRAW_QT_FACTORY_H
#define DRAW_QT_FACTORY_H

#include "draw/abstractfactory.h"
#include "qtdrawer.h"

class QtFactory : public AbstractFactory {
public:
    QtFactory(std::shared_ptr<QImage> img,
              std::shared_ptr<QPainter> painter, QColor pen) :
        img(img), painter(painter), pen(pen) {}

    ~QtFactory() = default;

    virtual std::unique_ptr<Drawer> create() override;

private:
    std::shared_ptr<QImage> img;
    std::shared_ptr<QPainter> painter;
    QColor pen;
};

#endif
