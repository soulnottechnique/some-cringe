#include "draw/qtdrawer.h"

void QtDrawer::drawLine(int x1, int y1, int x2, int y2)
{
    painter->setPen(pen);
    painter->drawLine(x1, y1, x2, y2);
}

void QtDrawer::clearCanvas()
{
    image->fill(Qt::black);
}
