#ifndef LOAD_CAMERA_DIRECTOR_H
#define LOAD_CAMERA_DIRECTOR_H

#include "load/loaddirector.h"
#include "load/camerabuilder.h"

class CameraDirector : public LoadDirector {
public:
    CameraDirector();

    ~CameraDirector() = default;

    virtual std::shared_ptr<Object> load() override;

private:
    std::shared_ptr<CameraBuilder> builder;
};

#endif
