#include "load/filecameraloader.h"

void FileCameraLoader::load(Dot &center)
{
    center = component->readDot();
}

void FileCameraLoader::load(Dir &direction)
{
    direction = component->readDir();
}

void FileCameraLoader::load(std::string &name)
{
    name = component->readString();
}
