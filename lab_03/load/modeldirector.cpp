#include "load/modeldirector.h"

ModelDirector::ModelDirector() :
    builder(std::make_shared<ModelBuilder>()) {}

std::shared_ptr<Object> ModelDirector::load()
{
    source->open();

    if (source->is_open())
    {
        std::string name;
        source->load(name);

        Dot center;
        source->load(center);

        std::vector<Dot> dots;
        source->load(dots);
        builder->buildDots(dots);

        std::vector<Edge> edges;
        source->load(edges);
        builder->buildEdges(edges);

        source->close();

        if (builder->isBuild())
        {
            std::shared_ptr<Units> un = builder->get();
            std::shared_ptr<Model> model = std::make_shared<Model>(center, name);

            model->units = un;

            return model;
        }
        else
            return nullptr;
    }

    return nullptr;
}
