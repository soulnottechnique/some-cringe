#ifndef LOAD_SOURCE_DECORATOR_H
#define LOAD_SOURCE_DECORATOR_H

#include <memory>
#include "load/sourceloader.h"

class SourceDecorator {
public:
    SourceDecorator(std::shared_ptr<SourceLoader> comp);

    virtual ~SourceDecorator() = 0;

    virtual void load(std::vector<Dot> &dots) = 0;
    virtual void load(std::vector<Edge> &edges) = 0;
    virtual void load(Dot &center) = 0;
    virtual void load(std::string &name) = 0;
    virtual void load(Dir &direction) = 0;

    void open() {component->open();}
    void close() {component->close();}
    bool is_open() {return component->is_open();}

protected:
    std::shared_ptr<SourceLoader> component;
};

#endif
