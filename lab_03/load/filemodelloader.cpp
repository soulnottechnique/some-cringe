#include "load/filemodelloader.h"

void FileModelLoader::load(std::vector<Dot> &dots)
{
    int n = component->readInt();
    while (n--)
        dots.push_back(component->readDot());
}

void FileModelLoader::load(std::vector<Edge> &edges)
{
    int n = component->readInt();
    while (n--)
        edges.push_back(component->readEdge());
}

void FileModelLoader::load(Dot &center)
{
    center = component->readDot();
}

void FileModelLoader::load(std::string &name)
{
    name = component->readString();
}
