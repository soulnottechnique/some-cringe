#ifndef LOAD_CAMERA_BUILDER_H
#define LOAD_CAMERA_BUILDER_H

#include <memory>
#include "object/camera.h"

class CameraBuilder {
public:
    CameraBuilder();

    bool isBuild();

    void buildCenter(Dot &center);
    void buildDirection(Dot &direction);
    void buildName(std::string &name);

    std::shared_ptr<Camera> get();
    void reset();

private:
    std::shared_ptr<Camera> camera;

    bool center, dir;
};

#endif
