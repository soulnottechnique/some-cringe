#include "load/sourcedecorator.h"

SourceDecorator::SourceDecorator(std::shared_ptr<SourceLoader> comp) :
    component(comp) {}

SourceDecorator::~SourceDecorator() = default;
