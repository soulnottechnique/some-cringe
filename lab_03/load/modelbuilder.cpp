#include "load/modelbuilder.h"

ModelBuilder::ModelBuilder() :
    model(std::make_shared<Units>()),
    dots(false), edges(false) {}


bool ModelBuilder::isBuild()
{
    return dots && edges;
}


void ModelBuilder::buildDots(std::vector<Dot> &dots)
{
    model->setDots(std::move(dots));
    this->dots = true;
}

void ModelBuilder::buildEdges(std::vector<Edge> &edges)
{
    model->setEdges(std::move(edges));
    this->edges = true;
}

std::shared_ptr<Units> ModelBuilder::get()
{
    if (this->isBuild())
        return model;
    else
        return nullptr;
}

void ModelBuilder::reset()
{
    model = std::make_shared<Units>();
}
