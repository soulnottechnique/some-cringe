#include "load/cameradirector.h"

CameraDirector::CameraDirector() :
    builder(std::make_shared<CameraBuilder>()) {}

std::shared_ptr<Object> CameraDirector::load()
{
    source->open();

    if (source->is_open())
    {
        std::string name;
        source->load(name);
        builder->buildName(name);

        Dot center;
        source->load(center);
        builder->buildCenter(center);

        Dot dir;
        source->load(dir);
        builder->buildDirection(dir);

        source->close();

        if (builder->isBuild())
            return builder->get();
        else
            return nullptr;
    }

    return nullptr;
}
