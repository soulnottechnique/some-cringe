#ifndef LOAD_MODEL_BUILDER_H
#define LOAD_MODEL_BUILDER_H

#include <memory>
#include "object/units.h"

class ModelBuilder {
public:
    ModelBuilder();

    bool isBuild();

    void buildDots(std::vector<Dot> &dots);
    void buildEdges(std::vector<Edge> &edges);

    std::shared_ptr<Units> get();
    void reset();

private:
    std::shared_ptr<Units> model;

    bool dots, edges;
};

#endif
