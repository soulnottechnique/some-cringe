#ifndef LOAD_FILE_CAMERA_LOADER_H
#define LOAD_FILE_CAMERA_LOADER_H

#include <vector>
#include "object/units.h"
#include "object/dir.h"
#include "load/sourcedecorator.h"

class FileCameraLoader : public SourceDecorator {
public:
    using SourceDecorator::SourceDecorator;

    virtual void load(Dot &center) override;
    virtual void load(Dir &direction) override;
    virtual void load(std::string &name) override;
    virtual void load(std::vector<Dot> &dots) override {}
    virtual void load(std::vector<Edge> &edges) override {}
};

#endif
