#ifndef LOAD_FILE_LOADER_H
#define LOAD_FILE_LOADER_H

#include <fstream>
#include "load/sourceloader.h"

class FileLoader : public SourceLoader {
public:
    FileLoader(std::string filename);

    ~FileLoader();

    virtual bool is_open();
    virtual void open();
    virtual void close();

    virtual int readInt();
    virtual float readFloat();
    virtual std::string readString();

    virtual Dot readDot();
    virtual Edge readEdge();

    virtual Dir readDir();

protected:
    std::ifstream input;
};

#endif
