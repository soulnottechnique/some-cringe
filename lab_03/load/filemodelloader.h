#ifndef LOAD_FILE_MODEL_LOADER_H
#define LOAD_FILE_MODEL_LOADER_H

#include <vector>
#include "object/units.h"
#include "load/sourcedecorator.h"

class FileModelLoader : public SourceDecorator {
public:
    using SourceDecorator::SourceDecorator;

    virtual void load(std::vector<Dot> &dots) override;
    virtual void load(std::vector<Edge> &edges)  override;
    virtual void load(Dot &center) override;
    virtual void load(std::string &name) override;
    virtual void load(Dir &direction) override {};
};

#endif
