#ifndef LOAD_LOAD_DIRECTOR_H
#define LOAD_LOAD_DIRECTOR_H

#include <memory>
#include "object/object.h"
#include "load/sourcedecorator.h"

class LoadDirector {
public:
    LoadDirector() = default;

    virtual ~LoadDirector() = default;

    virtual std::shared_ptr<Object> load() = 0;

public:
    virtual void setLoader(std::shared_ptr<SourceDecorator> source)
        {this->source = source;}

protected:
    std::shared_ptr<SourceDecorator> source;
};

#endif
