#ifndef LOAD_MODEL_DIRECTOR_H
#define LOAD_MODEL_DIRECTOR_H

#include "load/loaddirector.h"
#include "load/modelbuilder.h"
#include "object/model.h"

class ModelDirector : public LoadDirector {
public:
    ModelDirector();

    ~ModelDirector() = default;

    virtual std::shared_ptr<Object> load() override;

private:
    std::shared_ptr<ModelBuilder> builder;
};

#endif
