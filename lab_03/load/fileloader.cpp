#include "load/fileloader.h"

FileLoader::FileLoader(std::string filename) : SourceLoader(filename) {}

FileLoader::~FileLoader()
{
    if (input.is_open())
        input.close();
}

bool FileLoader::is_open()
{
    return input.is_open();
}

void FileLoader::open()
{
    input.open(sourceName);
}

void FileLoader::close()
{
    input.close();
}


int FileLoader::readInt()
{
    int x;
    input >> x;
    return x;
}

float FileLoader::readFloat()
{
    float x;
    input >> x;
    return x;
}

std::string FileLoader::readString()
{
    std::string x;
    input >> x;
    return x;
}

Dot FileLoader::readDot()
{
    float x, y, z;
    input >> x >> y >> z;
    return Dot(x, y, z);
}

Edge FileLoader::readEdge()
{
    int x, y;
    input >> x >> y;
    return Edge(x, y);
}

Dir FileLoader::readDir()
{
    float x, y;
    input >> x >> y;
    return Dir(x, y);
}
