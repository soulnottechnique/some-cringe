#ifndef LOAD_SOURCE_LOADER_H
#define LOAD_SOURCE_LOADER_H

#include <string>
#include "object/dir.h"
#include "object/units.h"

class SourceLoader {
public:
    SourceLoader(std::string sourceName) : sourceName(sourceName) {}

    virtual ~SourceLoader() = default;

    virtual bool is_open() = 0;
    virtual void open() = 0;
    virtual void close() = 0;

    virtual int readInt() = 0;
    virtual float readFloat() = 0;
    virtual std::string readString() = 0;

    virtual Dot readDot() = 0;
    virtual Edge readEdge() = 0;

    virtual Dir readDir() = 0;

protected:
    std::string sourceName;
};

#endif
