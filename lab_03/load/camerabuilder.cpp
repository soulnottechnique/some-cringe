#include "load/camerabuilder.h"

CameraBuilder::CameraBuilder() :
    camera(std::make_shared<Camera>(Dot())),
    center(false), dir(false) {}


bool CameraBuilder::isBuild()
{
    return center && dir;
}

void CameraBuilder::buildCenter(Dot &center)
{
    camera->move(center);
    this->center = true;
}

void CameraBuilder::buildDirection(Dot &direction)
{
    Dot dot;
    camera->rotate(dot, direction);
    this->dir = true;
}

void CameraBuilder::buildName(std::string &name)
{
    camera->setName(name);
}

std::shared_ptr<Camera> CameraBuilder::get()
{
    if (this->isBuild())
        return camera;
    else
        return nullptr;
}

void CameraBuilder::reset()
{
    camera = std::make_shared<Camera>(Dot());
}
