#ifndef MANAGER_CONFIG_MANAGER_H
#define MANAGER_CONFIG_MANAGER_H

#include <memory>
#include "manager/manager.h"
#include "solution/solution.h"
#include "solution/registrator.h"

class ConfigManager : public Manager {
public:
    ConfigManager();

    ~ConfigManager() = default;

    std::unique_ptr<LoaderCreator> getCreator(std::string id);
    std::unique_ptr<LoaderCreator> getCreatorFrom(std::string confname);

private:
    Solution solution;
    Registrator registrator;
};

#endif
