#ifndef MANAGER_TRANSFORM_MANAGER_H
#define MANAGER_TRANSFORM_MANAGER_H

#include "manager/manager.h"
#include "manager/scenemanagercreator.h"

class TransformManager : public Manager {
public:
    TransformManager(std::string name, std::shared_ptr<SceneManager> sceneManager);
    ~TransformManager() = default;

    void move(Dot &delta);
    void rotate(Dot &center, Dot &angle);
    void scale(Dot &center, Dot &ratio);

private:
    std::string name;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
