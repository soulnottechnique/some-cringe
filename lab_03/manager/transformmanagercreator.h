#ifndef MANAGER_TRANSFORM_MANAGER_CREATOR_H
#define MANAGER_TRANSFORM_MANAGER_CREATOR_H

#include "manager/transformmanager.h"

class TransformManagerCreator {
public:
    TransformManagerCreator(std::shared_ptr<SceneManager> sceneManager) :
        sceneManager(sceneManager) {}

    std::shared_ptr<TransformManager> create(std::string name);

private:
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
