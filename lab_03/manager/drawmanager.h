#ifndef MANAGER_DRAW_MANAGER_H
#define MANAGER_DRAW_MANAGER_H

#include <memory>
#include "manager/manager.h"
#include "manager/scenemanagercreator.h"
#include "visitor/visitor.h"

class DrawManager : public Manager {
public:
    DrawManager(std::shared_ptr<Drawer> drawer, std::shared_ptr<SceneManager> sceneManager);

    void draw();

private:
    Visitor visitor;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
