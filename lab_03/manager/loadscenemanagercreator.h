#ifndef MANAGR_LOAD_SCENE_MANAGER_CREATOR_H
#define MANAGR_LOAD_SCENE_MANAGER_CREATOR_H

#include "manager/loadscenemanager.h"

class LoadSceneManagerCreator {
public:
    std::shared_ptr<LoadSceneManager> create(std::string src);

private:
    std::shared_ptr<LoadSceneManager> mgr;
};

#endif
