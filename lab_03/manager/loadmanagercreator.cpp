#include "manager/loadmanagercreator.h"

std::shared_ptr<LoadManager> LoadManagerCreator::create(std::string conf,
                                                        std::string src)
{
    ConfigManagerCreator mgr;
    std::shared_ptr<ConfigManager> config = mgr.create();

    std::shared_ptr<LoadDirector> dir = config->getCreatorFrom(conf)->createLoader(src);

    std::shared_ptr<LoadManager> manager = std::make_shared<LoadManager>(dir);
    return manager;
}
