#include "manager/drawmanagercreator.h"

std::shared_ptr<DrawManager> DrawManagerCreator::create(std::shared_ptr<Drawer> drawer,
                              std::shared_ptr<SceneManager> sceneManager)
{
    if (!mgr)
        mgr = std::make_shared<DrawManager>(drawer, sceneManager);

    return mgr;
}
