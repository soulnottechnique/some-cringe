#ifndef MANAGER_LOAD_MANAGER_CREATOR_H
#define MANAGER_LOAD_MANAGER_CREATOR_H

#include "manager/loadmanager.h"
#include "manager/configmanagercreator.h"

class LoadManagerCreator {
public:

    std::shared_ptr<LoadManager> create(std::string conf, std::string src);

};

#endif
