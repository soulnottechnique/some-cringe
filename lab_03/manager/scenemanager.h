#ifndef MANAGER_SCENE_MANAGER_H
#define MANAGER_SCENE_MANAGER_H

#include "scene/scene.h"
#include "manager/manager.h"

class SceneManager : public Manager {
public:
    SceneManager() = default;
    ~SceneManager() = default;

    void setScene(std::shared_ptr<Scene> scene);
    std::shared_ptr<Scene> getScene();

    void setCamera(std::shared_ptr<Camera> watcher);
    std::shared_ptr<Camera> getCamera();

private:
    std::shared_ptr<Scene> scene;

    std::shared_ptr<Camera> watcher;
};

#endif
