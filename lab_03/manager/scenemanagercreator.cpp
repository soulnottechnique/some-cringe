#include "manager/scenemanagercreator.h"

std::shared_ptr<SceneManager> SceneManagerCreator::create()
{
    if (!mgr)
        mgr = std::make_shared<SceneManager>();

    return mgr;
}
