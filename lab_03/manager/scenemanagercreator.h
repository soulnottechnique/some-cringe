#ifndef MANAGER_SCENE_MANAGER_CREATOR_H
#define MANAGER_SCENE_MANAGER_CREATOR_H

#include "manager/scenemanager.h"

class SceneManagerCreator {
public:

    std::shared_ptr<SceneManager> create();

private:
    std::shared_ptr<SceneManager> mgr;
};

#endif
