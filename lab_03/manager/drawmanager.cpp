#include "manager/drawmanager.h"

DrawManager::DrawManager(std::shared_ptr<Drawer> drawer, std::shared_ptr<SceneManager> sceneManager) :
    visitor(drawer, sceneManager), sceneManager(sceneManager) {}

void DrawManager::draw()
{
    std::shared_ptr<Scene> scene = sceneManager->getScene();

    for (auto &obj : *scene)
        obj->accept(visitor);
}
