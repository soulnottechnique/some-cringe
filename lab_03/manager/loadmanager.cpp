#include "manager/loadmanager.h"

LoadManager::LoadManager(std::shared_ptr<LoadDirector> director) :
    director(director) {}

std::shared_ptr<Object> LoadManager::load()
{
    return director->load();
}
