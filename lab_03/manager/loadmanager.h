#ifndef MANAGER_LOAD_MANAGER_H
#define MANAGER_LOAD_MANAGER_H

#include "manager/manager.h"
#include "load/loaddirector.h"

class LoadManager : public Manager {
public:
    LoadManager(std::shared_ptr<LoadDirector> director);

    std::shared_ptr<Object> load();
private:
    std::shared_ptr<LoadDirector> director;
};

#endif
