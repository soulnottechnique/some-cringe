#include "manager/configmanager.h"

ConfigManager::ConfigManager()
{
    registrator.makeRegistration(solution);
}

std::unique_ptr<LoaderCreator> ConfigManager::getCreator(std::string id)
{
    return solution.create(id);
}

std::unique_ptr<LoaderCreator> ConfigManager::getCreatorFrom(std::string confname)
{
    std::ifstream input;

    input.open("C:/Users/acer/study/oop/lab_03/conf/" + confname);

    if (input.is_open())
    {
        std::string id;

        input >> id;
        std::unique_ptr<LoaderCreator> loader = getCreator(id);

        input.close();

        return loader;
    }

    return nullptr;
}
