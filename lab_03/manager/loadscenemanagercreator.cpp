#include "manager/loadscenemanagercreator.h"

std::shared_ptr<LoadSceneManager> LoadSceneManagerCreator::create(std::string src)
{
    if (!mgr)
    {
        std::shared_ptr<SourceLoader> source = std::make_shared<FileLoader>(src);
        std::shared_ptr<BaseSceneDirector> dir = std::make_shared<SceneDirector>(source);
        mgr = std::make_shared<LoadSceneManager>(dir);
    }

    return mgr;
}
