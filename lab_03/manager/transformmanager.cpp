#include "manager/transformmanager.h"

TransformManager::TransformManager(std::string name, std::shared_ptr<SceneManager> sceneManager) :
    name(name), sceneManager(sceneManager)
{
    //SceneManagerCreator creator;
    //mgr =  creator.create();
}

void TransformManager::move(Dot &delta)
{
    for (auto &obj : *(sceneManager->getScene()))
        if (obj->getName() == name)
            obj->move(delta);
}

void TransformManager::rotate(Dot &center, Dot &angle)
{
    for (auto &obj : *(sceneManager->getScene()))
        if (obj->getName() == name)
            obj->rotate(center, angle);
}

void TransformManager::scale(Dot &center, Dot &ratio)
{
    for (auto &obj : *(sceneManager->getScene()))
        if (obj->getName() == name)
            obj->scale(center, ratio);
}
