#include "manager/scenemanager.h"

void SceneManager::setScene(std::shared_ptr<Scene> scene)
{
    this->scene = scene;
}

std::shared_ptr<Scene> SceneManager::getScene()
{
    return scene;
}

void SceneManager::setCamera(std::shared_ptr<Camera> watcher)
{
    this->watcher = watcher;
}

std::shared_ptr<Camera> SceneManager::getCamera()
{
    return watcher;
}
