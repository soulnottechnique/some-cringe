#ifndef MANAGER_CONFIG_MANAGER_CREATOR_H
#define MANAGER_CONFIG_MANAGER_CREATOR_H

#include "manager/configmanager.h"

class ConfigManagerCreator {
public:
    std::shared_ptr<ConfigManager> create();

private:
    std::shared_ptr<ConfigManager> mgr;
};

#endif
