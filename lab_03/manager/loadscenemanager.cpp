#include "manager/loadscenemanager.h"

LoadSceneManager::LoadSceneManager(std::shared_ptr<BaseSceneDirector> director) :
    director(director) {}

std::shared_ptr<Scene> LoadSceneManager::load()
{
    return director->load();
}
