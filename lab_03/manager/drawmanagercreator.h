#ifndef MANAGER_DRAW_MANAGER_CREATOR
#define MANAGER_DRAW_MANAGER_CREATOR

#include <memory>
#include "manager/drawmanager.h"
#include "draw/drawer.h"

class DrawManagerCreator {
public:
    std::shared_ptr<DrawManager> create(std::shared_ptr<Drawer> drawer,
                                        std::shared_ptr<SceneManager> sceneManager);

private:
    std::shared_ptr<DrawManager> mgr;
};

#endif
