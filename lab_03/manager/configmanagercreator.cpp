#include "manager/configmanagercreator.h"

std::shared_ptr<ConfigManager> ConfigManagerCreator::create()
{
    if (!mgr)
        mgr = std::make_shared<ConfigManager>();

    return mgr;
}
