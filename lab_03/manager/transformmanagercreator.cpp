#include "transformmanagercreator.h"

std::shared_ptr<TransformManager> TransformManagerCreator::create(std::string name)
{
    return std::make_shared<TransformManager>(name, sceneManager);
}
