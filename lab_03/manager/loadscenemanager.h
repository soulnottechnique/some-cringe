#ifndef MANAGER_LOAD_SCENE_MANAGER_H
#define MANAGER_LOAD_SCENE_MANAGER_H

#include "scene/scenedirector.h"

class LoadSceneManager : public Manager {
public:
    LoadSceneManager(std::shared_ptr<BaseSceneDirector> director);

    std::shared_ptr<Scene> load();
private:
    std::shared_ptr<BaseSceneDirector> director;
};

#endif
