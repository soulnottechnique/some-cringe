#ifndef MANAGER_MANAGER_H
#define MANAGER_MANAGER_H

class Manager {
public:
    Manager() = default;

    virtual ~Manager() = default;
};

#endif
