#ifndef VISITOR_VISITOR_H
#define VISITOR_VISITOR_H

#include <memory>
#include "draw/drawer.h"
#include "object/dir.h"
#include "object/dot.h"

class Composite;
class Model;
class Camera;
class SceneManager;

class Visitor {
public:
    Visitor(std::shared_ptr<Drawer> drawer, std::shared_ptr<SceneManager> sceneManager) :
        drawer(drawer), sceneManager(sceneManager) {}

    void visit(Composite &comp);
    void visit(Model &mod);
    void visit(Camera &cam);

private:
    std::shared_ptr<SceneManager> sceneManager;
    std::shared_ptr<Drawer> drawer;
};

#endif
