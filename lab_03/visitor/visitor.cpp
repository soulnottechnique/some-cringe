#include "visitor/visitor.h"
#include "object/composite.h"
#include "object/model.h"
#include "object/camera.h"
#include "manager/scenemanagercreator.h"

void Visitor::visit(Composite &comp)
{
    for (auto &obj : comp)
        obj->accept(*this);
}

void Visitor::visit(Model &mod)
{
    std::shared_ptr<Camera> camera = sceneManager->getCamera();

    Dot center = camera->center;
    Dot normal = Dot(0, 0, 1).getRotated(camera->direction);
    Dot x = Dot(1, 0, 0).getRotated(camera->direction);
    Dot y = Dot(0, 1, 0).getRotated(camera->direction);

    std::vector<Dot> dots = mod.units->getDots();
    std::vector<Dot> proj;

    drawer->clearCanvas();

    for (auto &dot : dots)
    {
        Dot temp(dot.getX() - center.getX(), dot.getY() - center.getY(), dot.getZ() - center.getZ());
        float dist = normal.scalar(temp);

        temp = Dot(dot.getX() - normal.getX() * dist, dot.getY() - normal.getY() * dist, 0);

        float alpha = y.getX() * temp.getY() - y.getY() * temp.getX();
        float beta = x.getY() * temp.getX() - x.getX() * temp.getY();

        float div = y.getX() * x.getY() - x.getX() * y.getY();

        proj.push_back(Dot(alpha/div, beta/div, 0));
    }

    for (auto &edge : mod.units->getEdges())
    {
        Dot f = proj[edge.getFrom()], t = proj[edge.getTo()];
        drawer->drawLine(f.getX(), f.getY(), t.getX(), t.getY());
    }
}

void Visitor::visit(Camera &cam)
{

}
