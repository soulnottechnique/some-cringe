#ifndef COMMAND_ROTATE_COMMAND_H
#define COMMAND_ROTATE_COMMAND_H

#include "command/objectcommand.h"
#include "manager/transformmanagercreator.h"

class RotateCommand : public ObjectCommand {
public:
    RotateCommand(std::string name, Dot center, Dot angle, std::shared_ptr<SceneManager> sceneManager) :
        ObjectCommand(name), center(center), angle(angle),
        sceneManager(sceneManager) {}

    virtual ~RotateCommand() = default;

    virtual void execute() override;

private:
    Dot center, angle;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
