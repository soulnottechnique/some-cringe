#include "command/rotatecommand.h"

void RotateCommand::execute()
{
    TransformManagerCreator creator(sceneManager);
    creator.create(name)->rotate(center, angle);
}

