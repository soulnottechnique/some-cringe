#include "command/movecommand.h"

void MoveCommand::execute()
{
    TransformManagerCreator creator(sceneManager);
    creator.create(name)->move(delta);
}
