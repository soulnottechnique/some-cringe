#ifndef COMMAND_SCALE_COMMAND_H
#define COMMAND_SCALE_COMMAND_H

#include "command/objectcommand.h"
#include "manager/transformmanagercreator.h"

class ScaleCommand : public ObjectCommand {
public:
    ScaleCommand(std::string name, Dot center, Dot ratio, std::shared_ptr<SceneManager> sceneManager) :
        ObjectCommand(name), center(center), ratio(ratio),
        sceneManager(sceneManager) {}

    virtual ~ScaleCommand() = default;

    virtual void execute() override;

private:
    Dot center, ratio;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
