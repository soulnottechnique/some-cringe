#ifndef COMMAND_DRAW_COMMAND_H
#define COMMAND_DRAW_COMMAND_H

#include <memory>
#include "command/scenecommand.h"
#include "draw/drawer.h"
#include "manager/drawmanagercreator.h"

class DrawCommand : public SceneCommand {
public:
    DrawCommand(std::shared_ptr<Drawer> drawer, std::shared_ptr<SceneManager> sceneManager) :
        drawer(drawer), sceneManager(sceneManager) {}

    ~DrawCommand() = default;

    virtual void execute() override;

private:
    std::shared_ptr<Drawer> drawer;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
