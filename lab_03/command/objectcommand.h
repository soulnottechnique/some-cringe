#ifndef COMMAND_OBJECT_COMMAND_H
#define COMMAND_OBJECT_COMMAND_H

#include <string>
#include "command/command.h"

class ObjectCommand : public Command {
public:
    ObjectCommand(std::string name) : name(name) {}

    virtual ~ObjectCommand() = default;

    virtual void execute() = 0;

protected:
    std::string name;
};

#endif
