#ifndef COMMAND_MOVE_COMMAND_H
#define COMMAND_MOVE_COMMAND_H

#include "command/objectcommand.h"
#include "manager/transformmanagercreator.h"

class MoveCommand : public ObjectCommand {
public:
    MoveCommand(std::string name, Dot delta, std::shared_ptr<SceneManager> sceneManager) :
        ObjectCommand(name), delta(delta), sceneManager(sceneManager) {}

    virtual ~MoveCommand() = default;

    virtual void execute() override;

private:
    Dot delta;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
