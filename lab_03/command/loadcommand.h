#ifndef COMMAND_LOAD_COMMAND_H
#define COMMAND_LOAD_COMMAND_H

#include <string>
#include "command/scenecommand.h"
#include "manager/loadscenemanagercreator.h"
#include "manager/scenemanagercreator.h"

class LoadCommand : public SceneCommand {
public:
    LoadCommand(std::string src, std::shared_ptr<SceneManager> sceneManager) :
        src(src), sceneManager(sceneManager) {}

    ~LoadCommand() = default;

    virtual void execute() override;

private:
    std::string src;
    std::shared_ptr<SceneManager> sceneManager;
};

#endif
