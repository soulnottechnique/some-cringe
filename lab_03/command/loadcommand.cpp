#include "command/loadcommand.h"

void LoadCommand::execute()
{
    LoadSceneManagerCreator loadCreator;
    //SceneManagerCreator sceneCreator;

    static std::shared_ptr<LoadSceneManager> loadManager = loadCreator.create(src);
    //static std::shared_ptr<SceneManager> sceneManager = sceneCreator.create();

    std::shared_ptr<Scene> scene = loadManager->load();
    sceneManager->setScene(scene);

    for (auto obj : *scene)
        if (obj->isCamera())
            sceneManager->setCamera(std::dynamic_pointer_cast<Camera>(obj));

}
