#include "command/drawcommand.h"

void DrawCommand::execute()
{
    DrawManagerCreator creator;
    std::shared_ptr<DrawManager> mgr = creator.create(drawer, sceneManager);

    mgr->draw();
}
