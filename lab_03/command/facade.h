#ifndef COMMAND_FACADE_H
#define COMMAND_FACADE_H

#include <memory>
#include "command/command.h"

class Facade {
public:
    void executeCommand(std::shared_ptr<Command> cmd);
};

#endif
