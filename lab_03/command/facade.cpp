#include "command/facade.h"

void Facade::executeCommand(std::shared_ptr<Command> cmd)
{
    cmd->execute();
}
