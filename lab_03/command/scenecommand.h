#ifndef COMMAND_SCENE_COMMAND_H
#define COMMAND_SCENE_COMMAND_H

#include "command/command.h"

class SceneCommand : public Command {
public:
    SceneCommand() = default;

    virtual ~SceneCommand() = default;

    virtual void execute() = 0;
};

#endif
