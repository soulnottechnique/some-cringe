#include "command/scalecommand.h"

void ScaleCommand::execute()
{
    TransformManagerCreator creator(sceneManager);
    creator.create(name)->scale(center, ratio);
}
