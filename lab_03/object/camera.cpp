#include "object/camera.h"

Camera::Camera(Dot center) :
    InvisibleObject(center, "default"),
    direction(Dot()) {}


void Camera::move(Dot &delta)
{
    center.move(delta);
}

void Camera::rotate(Dot &center, Dot &direction)
{
    this->direction.move(direction);
}

void Camera::scale(Dot &center, Dot &ratio)
{
    return ;
}


bool Camera::isComposite() const
{
    return false;
}

bool Camera::isCamera() const
{
    return true;
}

void Camera::accept(Visitor &v)
{
    v.visit(*this);
}
