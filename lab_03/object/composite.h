#ifndef OBJECT_COMPOSITE_H
#define OBJECT_COMPOSITE_H

#include "object/object.h"

class Composite : public Object {
public:
    Composite(Dot center = Dot(), std::string name = "main");

    virtual void move(Dot &delta) override;
    virtual void rotate(Dot &center, Dot &angle) override;
    virtual void scale(Dot &center, Dot &ratio) override;

    virtual bool isComposite() const override;
    virtual bool isVisible() const final;
    virtual bool isCamera() const final;

    virtual void add(objPtr &obj) override;
    virtual void remove(objIterator &obj) override;

    virtual void accept(Visitor &v) override;

    virtual objIterator begin() override;
    virtual objIterator end() override;

private:
    objContainer objects;
};

#endif
