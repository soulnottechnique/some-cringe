#ifndef OBJECT_INVISIBLE_OBJECT_H
#define OBJECT_INVISIBLE_OBJECT_H

#include "object/object.h"

class InvisibleObject : public Object {
public:
    InvisibleObject();
    InvisibleObject(Dot &center, std::string name);

    virtual ~InvisibleObject() = default;

    virtual bool isVisible() const final;
};

#endif
