#include "object/edge.h"

Edge::Edge(size_t from, size_t to) :
    from(from), to(to) {}


void Edge::setLink(size_t from, size_t to)
{
    this->from = from;
    this->to = to;
}

void Edge::setFrom(size_t from)
{
    this->from = from;
}

void Edge::setTo(size_t to)
{
    this->to = to;
}


size_t Edge::getFrom() const
{
    return from;
}

size_t Edge::getTo() const
{
    return to;
}
