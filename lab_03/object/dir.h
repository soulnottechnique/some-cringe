#ifndef OBJECT_DIR_H
#define OBJECT_DIR_H

class Dir {
public:
    Dir(float alpha = 0, float beta = 0);

    void setDir(float alpha, float beta);
    void setAlpha(float alpha);
    void setBeta(float beta);

    float getAlpha() const;
    float getBeta() const;

    void rotate(Dir &angle);

private:
    float alpha, beta;
};

#endif
