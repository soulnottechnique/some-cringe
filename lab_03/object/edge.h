#ifndef OBJECT_EDGE_H
#define OBJECT_EDGE_H

#include <cstdlib>

class Edge {
public:
    Edge(size_t from, size_t to);

    void setLink(size_t from, size_t to);
    void setFrom(size_t from);
    void setTo(size_t to);

    size_t getFrom() const;
   size_t getTo() const;

private:
    size_t from, to;
};

#endif
