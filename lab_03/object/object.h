#ifndef OBJECT_OBJECT_H
#define OBJECT_OBJECT_H

#include <vector>
#include <string>
#include <memory>
#include <iterator>
#include "object/dot.h"
#include "object/units.h"
#include "visitor/visitor.h"

class Visitor;

class Object {
public:
    Object();
    Object(Dot &center, std::string name);

    virtual ~Object() = default;

    virtual void move(Dot &delta) = 0;
    virtual void rotate(Dot &center, Dot &angle) = 0;
    virtual void scale(Dot &center, Dot &ratio) = 0;

    virtual bool isComposite() const;
    virtual bool isVisible() const = 0;
    virtual bool isCamera() const = 0;

    using objPtr = std::shared_ptr<Object>;
    using objContainer = std::vector<objPtr>;
    using objIterator = objContainer::iterator;

    virtual void add(objPtr &obj);
    virtual void remove(objIterator &obj);

    virtual objIterator begin();
    virtual objIterator end();

    virtual void accept(Visitor &v) = 0;

    void setName(std::string name);
    std::string getName() {return name;}

protected:
    Dot center;
    std::string name;
};

#endif
