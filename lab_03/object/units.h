#ifndef OBJECT_UNITS_H
#define OBJECT_UNITS_H

#include <vector>
#include <cmath>
#include "object/dot.h"
#include "object/edge.h"

class Units {
public:
    void addDot(Dot &dot);
    void addEdge(Edge &edge);

    void setDots(std::vector<Dot> &&dots);
    void setEdges(std::vector<Edge> &&dots);

    std::vector<Dot> getDots() {return dots;}
    std::vector<Edge> getEdges() {return edges;}

    void move(Dot &delta);
    void rotate(Dot &center, Dot &angle);
    void scale(Dot &center, Dot &ratio);

private:
    std::vector<Dot> dots;
    std::vector<Edge> edges;
};

#endif
