#include "object/object.h"

Object::Object(Dot &center, std::string name) :
    center(center), name(name) {}

Object::Object() :
    center(0, 0, 0), name("default") {}

bool Object::isComposite() const
{
    return false;
}

void Object::setName(std::string name)
{
    this->name = name;
}


void Object::add(Object::objPtr &obj)
{
    return ;
}

void Object::remove(objIterator &obj)
{
    return ;
}


Object::objIterator Object::begin()
{
    return objIterator(nullptr);
}

Object::objIterator Object::end()
{
    return objIterator(nullptr);
}


