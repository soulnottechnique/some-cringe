#include "object/invisibleObject.h"

InvisibleObject::InvisibleObject(Dot &center, std::string name) :
    Object(center, name) {}

InvisibleObject::InvisibleObject() :
    Object() {}

bool InvisibleObject::isVisible() const
{
    return false;
}
