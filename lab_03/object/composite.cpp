#include "object/composite.h"

Composite::Composite(Dot center, std::string name) :
    Object(center, name) {}


void Composite::move(Dot &delta)
{
    for (auto &obj : objects)
        obj->move(delta);
}

void Composite::rotate(Dot &center, Dot &angle)
{
    for (auto &obj : objects)
        obj->rotate(center, angle);
}

void Composite::scale(Dot &center, Dot &ratio)
{
    for (auto &obj : objects)
        obj->scale(center, ratio);
}


bool Composite::isComposite() const
{
    return true;
}

bool Composite::isVisible() const
{
    return false;
}

bool Composite::isCamera() const
{
    return false;
}


void Composite::add(objPtr &obj)
{
    objects.push_back(obj);
}

void Composite::remove(objIterator &obj)
{
    objects.erase(obj);
}


Object::objIterator Composite::begin()
{
    return objects.begin();
}

Object::objIterator Composite::end()
{
    return objects.end();
}

void Composite::accept(Visitor &v)
{
    v.visit(*this);
}
