#ifndef OBJECT_DOT_H
#define OBJECT_DOT_H

#include <cmath>

class Dot {
public:
    Dot(float x = 0, float y = 0, float z = 0);

    void move(Dot &delta);
    void scale(Dot &center, Dot &ratio);
    void rotate(Dot &cos, Dot &sin);

    void setCoords(float x, float y, float z);
    void setX(float x);
    void setY(float y);
    void setZ(float z);

    float getX() const;
    float getY() const;
    float getZ() const;

    Dot neg() const;
    Dot getNormal();
    Dot getRotated(Dot &direction);
    float scalar(Dot &second);

private:
    float x, y, z;
};

#endif
