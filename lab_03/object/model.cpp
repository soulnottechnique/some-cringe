#include "object/model.h"

Model::Model(Dot &center, std::string name) :
    VisibleObject(center, name) {}

Model::Model() : VisibleObject() {}

void Model::move(Dot &delta)
{
    units->move(delta);
}

void Model::rotate(Dot &center, Dot &angle)
{
    units->rotate(center, angle);
}

void Model::scale(Dot &center, Dot &ratio)
{
    units->scale(center, ratio);
}


bool Model::isComposite() const
{
    return false;
}

bool Model::isCamera() const
{
    return false;
}

void Model::accept(Visitor &v)
{
    v.visit(*this);
}
