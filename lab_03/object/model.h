#ifndef OBJECT_MODEL_H
#define OBJECT_MODEL_H

#include "object/visibleObject.h"
#include "object/units.h"

class ModelDirector;

class Model : public VisibleObject {
public:
    Model();
    Model(Dot &center, std::string name);

    virtual void move(Dot &delta) override;
    virtual void rotate(Dot &center, Dot &angle) override;
    virtual void scale(Dot &center, Dot &ratio) override;

    virtual bool isComposite() const override;
    virtual bool isCamera() const final;

    virtual void accept(Visitor &v) override;

    friend ModelDirector;
    friend Visitor;

private:
    std::shared_ptr<Units> units;
};

#endif
