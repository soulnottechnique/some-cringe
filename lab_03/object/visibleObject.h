#ifndef OBJECT_VISIBLE_OBJECT_H
#define OBJECT_VISIBLE_OBJECT_H

#include "object/object.h"

class VisibleObject : public Object {
public:
    VisibleObject();
    VisibleObject(Dot &center, std::string name);

    virtual ~VisibleObject() = default;

    virtual bool isVisible() const final;
};

#endif
