#ifndef OBJECT_CAMERA_H
#define OBJECT_CAMERA_H

#include "object/dir.h"
#include "object/invisibleObject.h"

class Camera : public InvisibleObject {
public:
    Camera(Dot center);

    virtual void move(Dot &delta) override;
    virtual void rotate(Dot &center, Dot &direction) override;
    virtual void scale(Dot &center, Dot &ratio) override;

    virtual bool isComposite() const override;
    virtual bool isCamera() const final;

    virtual void accept(Visitor &v) override;

    friend Visitor;

private:
    Dot direction;
};

#endif
