#include "object/visibleObject.h"

VisibleObject::VisibleObject(Dot &center, std::string name) :
    Object(center, name) {}

VisibleObject::VisibleObject() :
    Object() {}

bool VisibleObject::isVisible() const
{
    return true;
}
