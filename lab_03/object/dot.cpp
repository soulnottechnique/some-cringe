#include "object/dot.h"

Dot::Dot(float x, float y, float z) : x(x), y(y), z(z) {}


void Dot::move(Dot &delta)
{
    this->x += delta.x;
    this->y += delta.y;
    this->z += delta.z;
}

void Dot::scale(Dot &center, Dot &ratio)
{
    this->x = ratio.x * this->x + center.x * (1.0 - ratio.x);
    this->y = ratio.y * this->y + center.y * (1.0 - ratio.y);
    this->z = ratio.z * this->z + center.z * (1.0 - ratio.z);
}

void Dot::rotate(Dot &cos, Dot &sin)
{
    float old = this->x;

    this->x = this->x * cos.z + this->y * sin.z;
    this->y = -old * sin.z + this->y * cos.z;

    old = this->x;
    this->x = this->x * cos.y + this->z * sin.y;
    this->z = -old * sin.y + this->z * cos.y;

    old = this->y;
    this->y = this->y * cos.x + this->z * sin.x;
    this->z = -old * sin.x + this->z * cos.x;
}

Dot Dot::neg() const
{
    return Dot(-x, -y, -z);
}


void Dot::setCoords(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

void Dot::setX(float x)
{
    this->x = x;
}

void Dot::setY(float y)
{
    this->y = y;
}
void Dot::setZ(float z)
{
    this->z = z;
}


float Dot::getX() const
{
    return x;
}

float Dot::getY() const
{
    return y;
}

float Dot::getZ() const
{
    return z;
}

Dot Dot::getNormal()
{
    Dot z(0, 0, 1);
    Dot cosA(cos(this->getX()), cos(this->getY()), cos(this->getZ()));
    Dot sinA(sin(this->getX()), sin(this->getY()), sin(this->getZ()));

    z.rotate(cosA, sinA);

    return z;
}

Dot Dot::getRotated(Dot &direction)
{
    Dot cosA(cos(direction.getX()), cos(direction.getY()), cos(direction.getZ()));
    Dot sinA(sin(direction.getX()), sin(direction.getY()), sin(direction.getZ()));

    Dot temp(*this);
    temp.rotate(cosA, sinA);

    return temp;
}

float Dot::scalar(Dot &second)
{
    return this->x * second.x + this->y * second.y + this->z * second.z;
}
