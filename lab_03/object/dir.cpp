#include "object/dir.h"

Dir::Dir(float alpha, float beta) :
    alpha(alpha), beta(beta) {}

void Dir::setDir(float alpha, float beta)
{
    this->alpha = alpha;
    this->beta = beta;
}

void Dir::setAlpha(float alpha)
{
    this->alpha = alpha;
}

void Dir::setBeta(float beta)
{
    this->beta = beta;
}

float Dir::getAlpha() const
{
    return alpha;
}

float Dir::getBeta() const
{
    return beta;
}

void Dir::rotate(Dir &angle)
{
    this->alpha += angle.alpha;
    this->beta += angle.beta;
}
