#include "object/units.h"

void Units::addDot(Dot &dot)
{
    dots.push_back(dot);
}

void Units::addEdge(Edge &edge)
{
    edges.push_back(edge);
}

void Units::setDots(std::vector<Dot> &&dots)
{
    this->dots = dots;
}

void Units::setEdges(std::vector<Edge> &&edges)
{
    this->edges = edges;
}


void Units::move(Dot &delta)
{
    for (auto &dot : dots)
        dot.move(delta);
}

void Units::rotate(Dot &center, Dot &angle)
{
    Dot negative = center.neg();
    Dot sinA(sin(angle.getX()), sin(angle.getY()), sin(angle.getZ()));
    Dot cosA(cos(angle.getX()), cos(angle.getY()), cos(angle.getZ()));

    this->move(negative);

    for (auto &dot : dots)
        dot.rotate(cosA, sinA);

    this->move(center);
}

void Units::scale(Dot &center, Dot &ratio)
{
    for (auto &dot : dots)
        dot.scale(center, ratio);
}
