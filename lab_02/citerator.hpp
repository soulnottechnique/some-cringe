#ifndef CITERATOR_HPP
#define CITERATOR_HPP

template<typename T>
CIterator<T>::CIterator() : ptr(nullptr) {}

template<typename T>
CIterator<T>::CIterator(const std::shared_ptr<Node<T>> &ptr)
{
    this->ptr = ptr;
}

template<typename T>
CIterator<T>::CIterator(const CIterator<T> &it)
{
    this->ptr = it.ptr;
}

template<typename T>
CIterator<T> CIterator<T>::next() const
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    return CIterator<T>(ptr.lock()->getNext());
}

template<typename T>
bool CIterator<T>::operator == (const CIterator<T> &it) const
{
    return this->ptr.lock() == it.ptr.lock();
}

template<typename T>
bool CIterator<T>::operator != (const CIterator<T> &it) const
{
    return this->ptr.lock() != it.ptr.lock();
}

template<typename T>
CIterator<T>& CIterator<T>::operator ++()
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    this->ptr = this->next().ptr;
    return *this;
}

template<typename T>
CIterator<T> CIterator<T>::operator ++(int)
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    CIterator<T> temp(*this);
    this->ptr = this->next().ptr;
    return temp;
}

template<typename T>
CIterator<T>& CIterator<T>::operator +=(int step)
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    while (step--) {
        this->ptr = this->next().ptr;
    }

    return *this;
}

template<typename T>
CIterator<T> CIterator<T>::operator +(int step) const
{
    CIterator<T> it(*this);
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    while (step--) {
        ++it;
    }

    return it;
}

template<typename T>
CIterator<T>& CIterator<T>::operator =(const CIterator<T> &iter)
{
    *this = CIterator<T>(iter);
    return *this;
}

template<typename T>
const T& CIterator<T>::operator *() const
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    return ptr.lock()->getData();
}

template<typename T>
const T* CIterator<T>::operator ->() const
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    return &(ptr.lock()->getData());
}

template<typename T>
CIterator<T>::operator bool() const
{
    return this->ptr.lock() != nullptr;
}

template<typename T>
bool CIterator<T>::is_invalid() const
{
    return ptr.expired();
}


#endif // CITERATOR_HPP
