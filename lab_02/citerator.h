#ifndef CITERATOR_H
#define CITERATOR_H

#include "errors.h"
#include "node.h"
#include "chrono"
#include <iterator>

using std::chrono::system_clock;

template<typename T>
class CIterator : std::iterator<std::forward_iterator_tag, T>
{
public:
    CIterator();
    CIterator(const std::shared_ptr<Node<T>> &ptr);
    CIterator(const CIterator<T> &it);
    ~CIterator() = default;

    CIterator<T> next() const;

    bool is_invalid() const;

    bool operator == (const CIterator<T> &it) const;
    bool operator != (const CIterator<T> &it) const;

    CIterator<T>& operator ++();
    CIterator<T> operator ++(int);

    CIterator<T> &operator +=(int step);
    CIterator<T> operator +(int step) const;

    CIterator<T> &operator =(const CIterator<T> &iter);

    const T& operator *() const;
    const T* operator ->() const;

    operator bool() const;

private:

    std::weak_ptr<Node<T>> ptr;
};

#include "citerator.hpp"

#endif // CITERATOR_H
