#ifndef ERRORS_HPP
#define ERRORS_HPP

listException::listException(const string &time, const string &fileName,
                   const string &className, const string &methodName)
{
    message = "\nTime: " + time + "\nFile: " + fileName + \
            "\nClass: " + className + "\nMethod:" + methodName;

    msg_c = message.c_str();
}

const char* listException::what() const noexcept
{
    return msg_c;
}

listAllocException::listAllocException(const string &time, const string &fileName,
                   const string &className, const string &methodName):
    listException(time, fileName, className, methodName)
{
    AllocMessage = "Alloc error\n" + message;
    alloc_msg_c = AllocMessage.c_str();
}

const char* listAllocException::what() const noexcept
{
    return alloc_msg_c;
}

listEmptyException::listEmptyException(const string &time, const string &fileName,
                   const string &className, const string &methodName):
    listException(time, fileName, className, methodName)
{
    EmptyMessage = "Empty list error\n" + message;
    empty_msg_c = EmptyMessage.c_str();
}

const char* listEmptyException::what() const noexcept
{
    return empty_msg_c;
}

listIteratorException::listIteratorException(const string &time, const string &fileName,
                   const string &className, const string &methodName):
    listException(time, fileName, className, methodName)
{
    IteratorMessage = "Iterator error\n" + message;
    iterator_msg_c = IteratorMessage.c_str();
}

const char* listIteratorException::what() const noexcept
{
    return iterator_msg_c;
}

#endif // ERRORS_HPP
