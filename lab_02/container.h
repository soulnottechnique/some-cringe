#ifndef CONTAINER_H
#define CONTAINER_H

#include "cstdlib"
#include "iterator.h"
#include "citerator.h"
#include "chrono"

class Container
{
public:
    Container() : size(0) {}
    virtual ~Container() = default;
    virtual size_t getSize() const = 0;
    virtual void clear() = 0;
protected:
    size_t size;
};

#endif // CONTAINER_H
