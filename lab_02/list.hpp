#ifndef LIST_HPP
#define LIST_HPP

template<typename T>
List<T>::List() : head(nullptr), tail(nullptr) {}

template<typename T>
List<T>::List(const List<T>& list)
{
    for (auto &item : list)
        this->push_back(item);
}

template<typename T>
List<T>::List(std::initializer_list<T> ilist)
{
    for (auto &item : ilist)
        this->push_back(item);
}

template<typename T>
List<T>::List(List<T>&& list) noexcept
{
    this->head = list.head;
    this->tail = list.tail;
    this->size = list.size;
}

template<typename T>
template<typename Iter>
List<T>::List(Iter begin, Iter end)
{
    for (auto it = begin; it != end; ++it)
        this->push_back(*it);
}

template<typename T>
List<T>::List(T *arr, size_t size)
{
    for (size_t i = 0; i < size; i++)
        this->push_back(arr[i]);

    this->size = size;
}

template<typename T>
void List<T>::push_back(const T val)
{
    insert(Iterator<T>(tail), val);
}

template<typename T>
void List<T>::push_front(const T val)
{
    std::shared_ptr<Node<T>> current;
    try
    {
        current = std::shared_ptr<Node<T>>(new Node<T>(val));
    }
    catch (std::bad_alloc &error)
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listAllocException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    current->setNext(this->head);

    this->head = current;
    if (!this->size)
        this->tail = current;

    this->size += 1;
}

template<typename T>
void List<T>::push_front(const List<T> &list)
{
    for (auto item : list)
        this->push_front(item);
}

template<typename T>
void List<T>::push_back(const List<T> &list)
{
    for (auto item : list)
        this->push_back(item);
}

template<typename T>
T List<T>::pop_front()
{
    return remove(begin());
}

template<typename T>
T List<T>::pop_back()
{
    return remove(Iterator<T>(tail));
}

template<typename T>
T List<T>::remove(Iterator<T> it)
{
    if (!this->size)
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listEmptyException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    if (it.is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    std::shared_ptr<Node<T>> p = getPtr(it);
    T data = p->getData();

    if (p == head)
    {
        head = p->getNext();
        return data;
    }

    if (p == tail)
    {
        std::shared_ptr<Node<T>> temp = head;
        while (temp->getNext() != p)
            temp = temp->getNext();

        tail = temp;
        return data;
    }

    Iterator<T> temp = begin();
    while (temp.next() != it)
        ++temp;

    std::shared_ptr<Node<T>> p_temp = getPtr(temp);
    p_temp->setNext(p->getNext());

    this->size -= 1;
    return data;
}

template<typename T>
void List<T>::remove(Iterator<T> start, Iterator<T> stop)
{
    Iterator<T> it = start, nxt;
    while (it != stop)
    {
        nxt = it.next();
        (void) this->remove(it);
        it = nxt;
    }
}

template<typename T>
void List<T>::remove(Iterator<T> start, size_t count)
{
    Iterator<T> it = start, nxt;
    for (size_t i = 0; i < count; i++)
    {
        nxt = it.next();
        (void) this->remove(it);
        it = nxt;
    }
}

template<typename T>
Iterator<T> List<T>::begin()
{
    return Iterator<T>(head);
}

template<typename T>
CIterator<T> List<T>::end() const
{
    return CIterator<T>(nullptr);
}

template<typename T>
CIterator<T> List<T>::begin() const
{
    return CIterator<T>(head);
}

template<typename T>
Iterator<T> List<T>::end()
{
    return Iterator<T>(nullptr);
}

template<typename T>
CIterator<T> List<T>::cbegin() const
{
    return CIterator<T>(head);
}

template<typename T>
CIterator<T> List<T>::cend() const
{
    return CIterator<T>(nullptr);
}

template<typename T>
void List<T>::insert(Iterator<T> it, const T &val)
{
    if (it.is_invalid() && this->size)
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    std::shared_ptr<Node<T>> current;
    try
    {
        current = std::shared_ptr<Node<T>>(new Node<T>(val));
    }
    catch (std::bad_alloc &err)
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listAllocException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    if (!this->size)
        this->head = this->tail = current;
    else
    {
        auto p_it = getPtr(it);
        std::shared_ptr<Node<T>> temp(p_it->getNext());

        p_it->setNext(current);
        current->setNext(temp);

        auto tmp = tail->getNext();
        if (tmp != nullptr)
            tail = tmp;
    }

    this->size += 1;
}

template<typename T>
void List<T>::insert(Iterator<T> it, const List<T> &list)
{
    if (it.is_invalid() && this->size)
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    List<T> copy(list);
    if (!this->size)
    {
        this->head = copy.head;
        this->tail = copy.tail;
    }
    else
    {
        auto p_it = getPtr(it);
        copy.tail->setNext(p_it->getNext());
        p_it->setNext(copy.head);

        if (it == Iterator<T>(this->tail))
            this->tail = copy.tail;
    }

    this->size += copy.size;
}

template<typename T>
template<typename Iter>
void List<T>::insert(Iterator<T> where, Iter what)
{
    this->insert(where, *what);
}

template<typename T>
template<typename Iter>
void List<T>::insert(Iterator<T> where, Iter start, Iter stop)
{
    for (auto it = start; it != stop; ++it)
    {
        this->insert(where, *it);
        ++where;
    }
}

template<typename T>
List<T>& List<T>::merge(List<T> &list)
{
    this->tail->setNext(list.head);
    this->size += list.size;

    list.clear();
    return *this;
}

template<typename T>
size_t List<T>::getSize() const
{
    return this->size;
}

template<typename T>
void List<T>::clear()
{
    head.reset();
    tail.reset();
    this->size = 0;
}

template<typename T>
List<T> List<T>::operator +(const List<T> &list) const
{
    List<T> temp;
    temp.push_back(*this);
    temp.push_back(list);
    return temp;
}

template<typename T>
List<T>& List<T>::operator +=(const List<T> &list)
{
    insert(Iterator<T>(tail), list);
    return *this;
}

template<typename T>
List<T>& List<T>::operator =(const List<T> &list)
{
    this->clear();
    this->push_back(list);
    return *this;
}

template<typename T>
List<T>& List<T>::operator =(List<T> &&list)
{
    this->clear();
    this->head = list.head;
    this->tail = list.tail;
    this->size = list.size;
    return *this;
}

template<typename T>
List<T>& List<T>::operator =(std::initializer_list<T> ilist)
{
    this->clear();
    for (auto &it : ilist)
        this->push_back(it);
    return *this;
}

template<typename T>
bool List<T>::operator ==(const List<T> &list) const
{
    if (this->size != list.size)
        return false;

    auto it1 = this->begin(), it2 = list.begin();
    while (it1 != this->end())
    {
        if (*it1 != *it2)
            return false;

        ++it1;
        ++it2;
    }

    return true;
}

template<typename T>
List<T>::operator bool() const
{
    return this->size == 0;
}

template<typename T>
std::shared_ptr<Node<T>> List<T>::getPtr(Iterator<T> &it) const
{
    return it.ptr.lock();
}

template<typename T>
void List<T>::push_back(std::initializer_list<T> ilist)
{
    for (auto &item : ilist)
        this->push_back(item);
}

template<typename T>
void List<T>::push_front(std::initializer_list<T> ilist)
{
    for (auto &item : ilist)
        this->push_front(item);
}

template<typename T>
std::ostream& operator <<(std::ostream &out, List<T> &list)
{
    out << "{ ";

    for (auto &item : list)
        out << item << " ";

    out << "}";
    return out;
}

template<typename T>
bool List<T>::operator !=(const List<T> &list) const
{
    return !(*this == list);
}

template<typename T>
bool List<T>::is_empty() const
{
    return this->size == 0;
}

#endif // LIST_HPP
