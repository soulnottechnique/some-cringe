#include <iostream>

using namespace std;

#include "list.h"

List<int> f() {return List<int>({2, 3, 4});}

void list_constructor()
{
    List<int> a {1, 2, 3};
    cout << a << endl;

    List<int> b(a);
    cout << b << endl;

    List<int> c(a.begin(), a.end());
    cout << c << endl;

    List<double> z = List<double>();
    cout << z << endl;

    List<int> ret = f();
    cout << ret << endl;

    int arr[5] = {1, 2, 3, 4, 5};
    List<int> t(arr, 5);
    cout << t << endl;
}

void push()
{
    List<int> a;
    a.push_back(1);

    List<int> b {2, 3};
    a.push_back(b);

    a.push_back({4, 5, 6});

    cout << a << endl;

    a.clear();

    a.push_front(1);

    a.push_front(b);

    a.push_front({4, 5, 6});

    cout << a << endl;
}

void pop()
{
    List<int> a {1, 2, 3, 4, 5, 6};

    cout << a.pop_front() << endl;
    cout << a.pop_back() << endl;

    cout << a.pop_front() << endl;
    cout << a.pop_back() << endl;
}

void insert()
{
    List<int> a {1, 2, 3, 4, 5};

    a.insert(a.begin(), -1);
    cout << a << endl;

    List<int> b {-1, -2, -3};
    a.insert(a.begin() + 2, b);
    cout << a << endl;

    a.insert(a.begin(), b.begin());
    cout << a << endl;
}

void  exepts()
{
    List<int> a {};

    try {
        a.insert(a.end() + 1, 2);
    }  catch (listIteratorException &err) {
        cout << err.what() << endl;
    }

    try {
        a.pop_back();
    }  catch (listEmptyException &err) {
        cout << err.what() << endl;
    }
}

void remove()
{
    List<int> a {1, 2, 3, 4, 5};
    a.remove(a.begin() + 2);
    cout << a << endl;

    a.remove(a.begin(), a.end());
    cout << a << endl;

    a.push_back({1, 2});
    a.remove(a.begin(), 2);
    cout << a << endl;
}

void list_operator()
{
    List<int> a {1, 2, 3};
    List<int> b {4, 5, 6};

    List<int> c = a + b;
    cout << c << endl;

    a += b;
    cout << a << endl;

    a = {1, 2, 3};
    cout << a << endl;
}

void merge()
{
    List<int> a {1, 2, 3}, b {4, 5, 6};
    a.merge(b);

    cout << a << endl;
    cout << b << endl;
}

void iter()
{
    List<int> a {1, 2, 3};

    Iterator<int> it = a.begin();

    cout << int(it == a.begin()) << " " << int(it != a.end()) << endl;

    cout << *(it++) << endl;
    cout << *it << endl;

    cout << *(++it) << endl;
    cout << *it << endl;

    cout << *(a.begin() + 1) << endl;

    cout << bool(a.begin()) << " " << bool(a.end()) << endl;
}

void c_iter()
{
    const List<int> a {1, 2, 3};

    CIterator<int> it = a.cbegin();

    cout << int(it == a.cbegin()) << " " << int(it != a.cend()) << endl;

    cout << *(it++) << endl;
    cout << *it << endl;

    cout << *(++it) << endl;
    cout << *it << endl;

    cout << *(a.cbegin() + 1) << endl;

    cout << bool(a.cbegin()) << " " << bool(a.cend()) << endl;
}

int main()
{
    list_constructor();
    push();
    pop();
    insert();
    list_operator();
    merge();
    iter();
    c_iter();
    exepts();
    return 0;
}
