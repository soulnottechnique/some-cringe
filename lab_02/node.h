#ifndef NODE_H
#define NODE_H

#include <memory>

template<typename T>
class Node
{
public:
    Node(const T val);

    Node(const Node& node) = delete;
    Node(Node&& node) = delete;

    ~Node() = default;

    T& getData();
    std::shared_ptr<Node<T>> getNext() const;

    void setData(const T val);
    void setNext(const std::shared_ptr<Node<T>> nextNode);

private:
    T data;
    std::shared_ptr<Node<T>> next;
};

#include "node.hpp"

#endif // NODE_H
