﻿#ifndef LIST_H
#define LIST_H

#include <iostream>

#include "container.h"
#include "node.h"
#include "errors.h"

using std::chrono::system_clock;

template<typename T>
class List: Container
{
public:
    List();
    List(List<T>&& list) noexcept;
    List(T *arr, size_t size);
    List(std::initializer_list<T> ilist);
    explicit List(const List<T>& list);
    template<typename Iter>
    List(Iter start, Iter stop);

    virtual ~List() = default;

    virtual size_t getSize() const override;
    virtual void clear() override;

    Iterator<T> begin();
    Iterator<T> end();

    CIterator<T> begin() const;
    CIterator<T> end() const;

    CIterator<T> cbegin() const;
    CIterator<T> cend() const;

    void insert(Iterator<T> it, const T &val);
    void insert(Iterator<T> it, const List<T> &list);

    template<typename Iter>
    void insert(Iterator<T> where, Iter what);

    template<typename Iter>
    void insert(Iterator<T> where, Iter start, Iter stop);

    T remove(Iterator<T> it);
    void remove(Iterator<T> start, Iterator<T> stop);
    void remove(Iterator<T> start, size_t count);

    List<T>& merge(List<T> &list);

    void push_front(const T val);
    void push_front(const List<T> &list);
    void push_front(std::initializer_list<T> ilist);

    void push_back(const T val);
    void push_back(const List<T> &list);
    void push_back(std::initializer_list<T> ilist);

    T pop_front();
    T pop_back();

    List<T> operator +(const List<T> &list) const;
    List<T>& operator +=(const List<T> &list);

    List<T>& operator =(const List<T> &list);
    List<T>& operator =(List<T> &&list);
    List<T>& operator =(std::initializer_list<T> ilist);

    bool operator ==(const List<T> &list) const;
    bool operator !=(const List<T> &list) const;

    explicit operator bool() const;
    bool is_empty() const;

protected:
    friend Iterator<T>;
    std::shared_ptr<Node<T>> getPtr(Iterator<T> &it) const;

private:
    std::shared_ptr<Node<T>> head, tail;
};

#include "list.hpp"

#endif // LIST_H
