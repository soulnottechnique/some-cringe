#ifndef NODE_HPP
#define NODE_HPP

template<typename T>
Node<T>::Node(const T val) : data(val) {}

template<typename T>
T& Node<T>::getData()
{
    return this->data;
}

template<typename T>
std::shared_ptr<Node<T>> Node<T>::getNext() const
{
    return this->next;
}

template<typename T>
void Node<T>::setData(const T val)
{
    this->data = val;
}

template<typename T>
void Node<T>::setNext(const std::shared_ptr<Node<T>> nextNode)
{
    this->next = nextNode;
}

#endif // NODE_HPP
