#ifndef ITERATOR_H
#define ITERATOR_H

#include "node.h"
#include "errors.h"
#include "chrono"
#include <iterator>

using std::chrono::system_clock;

template<typename T>
class List;

template<typename T>
class Iterator : std::iterator<std::forward_iterator_tag, T>
{
public:
    Iterator();
    Iterator(const std::shared_ptr<Node<T>> &ptr);
    Iterator(const Iterator<T> &it);
    ~Iterator() = default;

    Iterator<T> next() const;

    bool is_invalid() const;

    bool operator == (const Iterator<T> &it) const;
    bool operator != (const Iterator<T> &it) const;

    Iterator<T>& operator ++();
    Iterator<T> operator ++(int);

    Iterator<T> &operator +=(int step);
    Iterator<T> operator +(int step) const;

    Iterator<T> &operator =(const Iterator<T> &iter);

    T& operator *();
    T* operator ->();
    const T& operator *() const;
    const T* operator ->() const;

    explicit operator bool() const;

protected:
    friend std::shared_ptr<Node<T>> List<T>::getPtr(Iterator<T> &it) const;

private:
    std::weak_ptr<Node<T>> ptr;
};

#include "iterator.hpp"

#endif // ITERATOR_H
