#ifndef ERRORS_H
#define ERRORS_H

#include <exception>
#include <string>

using namespace std;

class listException: std::exception
{
public:
    listException(const string &time, const string &fileName,
                       const string &className, const string &methodName);

    virtual const char *what() const noexcept override;

protected:
    string message;
    const char *msg_c;
};

class listAllocException: listException
{
public:
    listAllocException(const string &time, const string &fileName,
                       const string &className, const string &methodName);

    virtual const char *what() const noexcept override;

private:
    string AllocMessage;
    const char *alloc_msg_c;
};

class listEmptyException: listException
{
public:
    listEmptyException(const string &time, const string &fileName,
                       const string &className, const string &methodName);

    virtual const char *what() const noexcept override;

private:
    string EmptyMessage;
    const char *empty_msg_c;
};

class listIteratorException: listException
{
public:
    listIteratorException(const string &time, const string &fileName,
                       const string &className, const string &methodName);

    virtual const char *what() const noexcept override;

private:
    string IteratorMessage;
    const char *iterator_msg_c;
};

#include "errors.hpp"

#endif // ERRORS_H
