#ifndef ITERATOR_HPP
#define ITERATOR_HPP

template<typename T>
Iterator<T>::Iterator() {}

template<typename T>
Iterator<T>::Iterator(const Iterator<T> &it)
{
    this->ptr = it.ptr;
}

template<typename T>
Iterator<T>::Iterator(const std::shared_ptr<Node<T>> &ptr)
{
    this->ptr = ptr;
}

template<typename T>
bool Iterator<T>::operator == (const Iterator<T> &it) const
{
    return this->ptr.lock() == it.ptr.lock();
}

template<typename T>
bool Iterator<T>::operator != (const Iterator<T> &it) const
{
    return this->ptr.lock() != it.ptr.lock();
}

template<typename T>
Iterator<T> Iterator<T>::next() const
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    return Iterator<T>(ptr.lock()->getNext());
}

template<typename T>
const T& Iterator<T>::operator *() const
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    return ptr.lock()->getData();
}

template<typename T>
Iterator<T>& Iterator<T>::operator ++()
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    this->ptr = ptr.lock()->getNext();
    return *this;
}

template<typename T>
const T* Iterator<T>::operator ->() const
{
    return &(ptr.lock()->getData());
}

template<typename T>
Iterator<T>::operator bool() const
{
    return this->ptr.lock() != nullptr;
}

template<typename T>
Iterator<T> Iterator<T>::operator ++(int)
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    Iterator<T> temp(*this);
    this->ptr = ptr.lock()->getNext();
    return temp;
}

template<typename T>
Iterator<T>& Iterator<T>::operator +=(int step)
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    while (step--) {
        this->ptr = this->next().ptr;
    }

    return *this;
}

template<typename T>
Iterator<T> Iterator<T>::operator +(int step) const
{
    Iterator<T> it(*this);

    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }

    while (step--) {
        ++it;
    }

    return it;
}

template<typename T>
Iterator<T>& Iterator<T>::operator =(const Iterator<T> &iter)
{
    *this = Iterator<T>(iter);
    return *this;
}

template<typename T>
bool Iterator<T>::is_invalid() const
{
    return ptr.expired();
}

template<typename T>
T& operator *()
{
    if (is_invalid())
    {
        auto timenow = system_clock::to_time_t(system_clock::now());
        throw listIteratorException(ctime(&timenow), __FILE__, typeid(this).name(), __FUNCTION__);
    }
    return ptr.lock()->getData();
}

template<typename T>
T* operator ->()
{
    return &(ptr.lock()->getData());
}

#endif // ITERATOR_HPP
